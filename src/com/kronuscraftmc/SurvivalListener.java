package com.kronuscraftmc;

import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.kronuscraftmc.inventorys.HelpInventory;
import com.kronuscraftmc.inventorys.RankupInventory;
import com.kronuscraftmc.inventorys.credits.CreditMasterInventory;
import com.kronuscraftmc.inventorys.donate.DonateInventory;
import com.kronuscraftmc.kronuscraft.KronusCraft;
import com.kronuscraftmc.kronuscraft.UpdateEvent;
import com.kronuscraftmc.kronuscraft.database.CustomPlayerData;
import com.kronuscraftmc.kronuscraft.database.SurvivalPlayerData;
import com.kronuscraftmc.kronuscraft.players.GamePlayer;
import com.kronuscraftmc.kronuscraft.util.UpdateType;
import com.kronuscraftmc.shops.ShopManager;
import com.kronuscraftmc.shops.ShopNav;
import com.vexsoftware.votifier.model.VotifierEvent;

import net.citizensnpcs.api.event.NPCRightClickEvent;

public class SurvivalListener implements Listener {
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {	
		Player player = e.getPlayer();
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(Survival.getInstance(), new Runnable() {
			@Override
			public void run() {
				SurvivalPlayer survivalPlayer = new SurvivalPlayer(player.getUniqueId());
				SurvivalPlayerManager.addPlayer(survivalPlayer);
				
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7--------------- &bKronusCraft Network &r&7---------------"));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "                     &7Welcome " + player.getName() + "&f!"));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "   &e&l  Earn Money &8--> &e&lRank Up &8--> &e&lExtra Perks"));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "         &cRank up &8| &cMcMMO &8| &cJobs &8| &cEconomy &8| &cShops"));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "    &cAuction &8| &cCrates &8| &cGrief Proof &8| &cCustom Developed"));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "  &7Voting earns &aMoney&7, &aVote Crate Keys &7and &aVote Credits&7!"));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "           &7Use command &a/vote&7 for voting links!"));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "   &oUse &a&o/buy&f&o to support the development of the Network!"));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7--------------- &bKronusCraft Network &r&7---------------"));

				player.setPlayerListHeader(ChatColor.translateAlternateColorCodes('&', "&b&lKronusCraft Network &c&o1.13.2"));
				player.setPlayerListFooter(ChatColor.translateAlternateColorCodes('&',"&8&oVote for us:&r &a/vote" + " \n " + "&8&oWebsite: &ewww.kronuscraftmc.com"));
			}	
			
		}, 5L);

		if(!player.hasPlayedBefore()) {
			Survival.getInstance().getEconomy().bankDeposit(player.getName(), 250.0);
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crates key " + player.getName() + " mystery");
			player.teleport(new Location(Bukkit.getWorld("world"), -41.5, 98, -9.5));
		}
		
		ShopManager shopManager = Survival.getInstance().getShopManager();
		
		// track player's  shop preferences if they aren't being tracked already
		if(!shopManager.tracking(player)) {
			shopManager.trackPlayer(player);
		}
		
		// reset player's sellAll preference when they login
		shopManager.resetSellAllPreference(player);
	}

	@EventHandler (priority = EventPriority.HIGHEST)
	public void onPlayerQuit(PlayerQuitEvent e) {
		SurvivalPlayer player = SurvivalPlayerManager.getPlayer(e.getPlayer().getUniqueId());
		SurvivalPlayerManager.removePlayer(player);
	}


	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		e.setDeathMessage(null);
	}
	
	@EventHandler (priority = EventPriority.HIGHEST)
	public void onPlayerChatEvent(AsyncPlayerChatEvent e) {
		GamePlayer p = KronusCraft.getInstance().getPlayerManager().getGamePlayer(e.getPlayer().getUniqueId());
		SurvivalPlayer player = SurvivalPlayerManager.getPlayer(e.getPlayer().getName());
		e.setFormat(player.getRank().getTag(false, false, player.getRank().getChatColor()) + p.getRank().getTag(false, false, p.getRank().getChatColor()) + "%s" + ChatColor.WHITE + ": " + p.getRank().getTextColor(p) + "%s");

	}

	@EventHandler
	public void onUpdate(UpdateEvent e) {
		if (e.getType() == UpdateType.SLOW) {
			for (SurvivalPlayer players : SurvivalPlayerManager.getPlayers()) {
				int money = (int) players.getMoney();
				int credits = players.getGamePlayer().getVoteCredits();
				int rank = players.getRank().getId();
				int votes = Survival.getInstance().getVoteParty().getRemainingVotes();
				if (!players.checkCredits(credits)) {
					players.setScoreboardCredits(credits);
					players.setupScoreboard();
				}
				if(!players.checkRank(rank)) {
					players.setScoreboardRank(rank);
				}
				if (!players.checkMoney(money)) {
					players.setScoreboardMoney(money);
					players.setupScoreboard();
					players.setupBossBars();
				}
				if (!players.checkVotes(votes)) {
					players.setScoreboardVotes(votes);
					players.setupScoreboard();
				}
				if(Survival.getInstance().getGuildsAPI().getGuild(players.getPlayer()) != null) {
					if (!players.checkGuildName(Survival.getInstance().getGuildsAPI().getGuild(players.getPlayer()))) {
						players.setGuildName(Survival.getInstance().getGuildsAPI().getGuild(players.getPlayer()));
						players.setupScoreboard();
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onCommandEvent(PlayerCommandPreprocessEvent e) {
		if(e.getMessage().equalsIgnoreCase("/buy")) {
			DonateInventory.INVENTORY.open(e.getPlayer());
		} else if(e.getMessage().equalsIgnoreCase("/wild") || e.getMessage().equalsIgnoreCase("/rtp")) {
			if(e.getPlayer().getWorld().getName().equals("world_nether")) {
				e.setCancelled(true);
				e.getPlayer().sendMessage(ChatColor.RED + "Command disabled in this world!");
			}
		}
	}

	@EventHandler
	public void onNPCRightClick(NPCRightClickEvent p) {
		if (p.getNPC().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&eWild William"))) {
			p.getClicker().performCommand("wild");
		} else if(p.getNPC().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&eHelpful Harry"))) {
			HelpInventory.INVENTORY.open(p.getClicker());
		} else if(p.getNPC().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&eMiner Mack"))) {
			p.getClicker().performCommand("warp mine");
			p.getClicker().sendMessage(ChatColor.GRAY + "You're being teleported to the mine!...");
			p.getClicker().sendMessage(ChatColor.GRAY + "Use command " + ChatColor.AQUA + "/mine" + ChatColor.GRAY + " at any point!");
		} else if(p.getNPC().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&eShop"))) {
			ShopNav.INVENTORY.open(p.getClicker());
		}  else if(p.getNPC().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&eRank Master"))) {
			RankupInventory.INVENTORY.open(p.getClicker());
		} else if(p.getNPC().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&eDonate Master"))) {
			DonateInventory.INVENTORY.open(p.getClicker());
			p.getClicker().sendMessage(ChatColor.RED + "Type " + ChatColor.AQUA + "/buy" + ChatColor.RED + " in chat to view our Shop!");
		} else if(p.getNPC().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&eKit Master"))) {
			p.getClicker().performCommand("kit");
		} else if(p.getNPC().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&eCredit Master"))) {
			CreditMasterInventory.INVENTORY.open(p.getClicker());
		} else if(p.getNPC().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&eAuto Machines"))) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "im openbook " + p.getClicker().getName());
		} else if(p.getNPC().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&eJob Master"))) {
			p.getClicker().performCommand("jobs browse");
		} else if(p.getNPC().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&eGuild Master"))) {
			p.getClicker().sendMessage("For a list of commands use " + ChatColor.AQUA + "/guild help");
			p.getClicker().sendMessage("Use command " + ChatColor.AQUA + "/guild create <name> <prefix>" + ChatColor.WHITE + " to create a guild!");
			p.getClicker().sendMessage(ChatColor.GRAY + "" + ChatColor.ITALIC + "Guilds cost " + ChatColor.GREEN + "$2,500" + ChatColor.GRAY + ChatColor.ITALIC + " to create a guild.");
		}
	}
	
	@EventHandler
	public void onVote(VotifierEvent e) {
		String name = e.getVote().getUsername();
		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&7[&aKC&7]&b " + name + "&7 has been awarded &a1 Voter Key&7 & &a$300&7!"));
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + name + " voter");
		Survival.getInstance().getVoteParty().addVote();
		
		Random rand = new Random();
		int value = rand.nextInt(100);
		if(value < 6) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + name + " epic");
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&7[&aKC&7]&b " + name + "&7 got lucky and gained &a1 Epic Key&7!"));
		} else if (value < 3) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + name + " legendary");
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&7[&aKC&7]&b " + name + "&7 got lucky and gained &a1 Legendary Key&7!"));
		} else if(value < 10) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + name + " 1000");
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&7[&aKC&7]&b " + name + "&7 got lucky and gained &a$1,000&7!"));
		}
		
		if(SurvivalPlayerData.checkPlayerExists(name)) {
			UUID uuid = CustomPlayerData.getGamePlayerUUID(name);
			GamePlayer player = null;
			if (!Bukkit.getPlayer(name).isOnline()) {
				player = KronusCraft.getInstance().getPlayerManager().getGamePlayer(uuid);
			} else {
				player = new GamePlayer(name);
			}
			if(player != null) {
				player.setVoteCredits(player.getVoteCredits() + 1);
				if(player.getRank().getId() > 0) {
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + name + " 600");
				}
			}
		}
	}
}
