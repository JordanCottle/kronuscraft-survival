package com.kronuscraftmc.shops.defaults;

import org.bukkit.configuration.file.FileConfiguration;

public enum Nether {
	NETHERRACK("NETHERRACK", 2.0, 0.25),
	NETHER_BRICKS("NETHER_BRICKS", 3.0, 1.0),
	NETHER_BRICK_FENCE("NETHER_BRICK_FENCE", 5.0, 1.0),
	GLOWSTONE("GLOWSTONE", 10.0, 3.0),
	NETHER_WART("NETHER_WART", 3.0, 1.50),
	PURPUR_BLOCK("PURPUR_BLOCK", 5.0, 1.50),
	PURPUR_PILLAR("PURPUR_PILLAR", 5.0, 1.50),
	RED_NETHER_BRICKS("RED_NETHER_BRICKS", 5.0, 1.50),
	SOUL_SAND("SOUL_SAND", 3.0, 0.50),
	END_STONE("END_STONE", 5.0, 0.50),
	MAGMA_BLOCK("MAGMA_BLOCK", 5.0, 2.0),
	QUARTZ_BLOCK("QUARTZ_BLOCK", 5.0, 1.0);
	
	public final String name;
	public final double buyPrice;
	public final double sellPrice;
	Nether(String name, double buyPrice, double sellPrice){
		this.name = name;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
	}
	
	public static void setUp(FileConfiguration fileConfig) {
		for(Nether item: Nether.values()) {
			fileConfig.addDefault(item.name + ".BuyPrice", item.buyPrice);
			fileConfig.addDefault(item.name + ".SellPrice", item.sellPrice);
		}
	}
}
