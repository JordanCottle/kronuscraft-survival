package com.kronuscraftmc;

import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.kronuscraftmc.commands.CreditsCommand;
import com.kronuscraftmc.commands.HelpCommand;
import com.kronuscraftmc.commands.MachinesCommand;
import com.kronuscraftmc.commands.MineCommand;
import com.kronuscraftmc.commands.RanksCommand;
import com.kronuscraftmc.commands.ShopsCommand;
import com.kronuscraftmc.kronuscraft.KronusCraft;
import com.kronuscraftmc.kronuscraft.chat.Announcer;
import com.kronuscraftmc.kronuscraft.chat.Message;
import com.kronuscraftmc.ranks.RankupCommand;
import com.kronuscraftmc.shops.ShopManager;

import fr.minuskube.inv.InventoryManager;
import me.glaremasters.guilds.api.GuildsAPI;
import net.milkbowl.vault.economy.Economy;

public class Survival extends JavaPlugin {

	private static Survival instance;
	public static KronusCraft kronus;
	private Economy economy;
	private InventoryManager invManager;
	private ShopManager shopManager;
	private VoteParty voteParty;
	private GuildsAPI guildsAPI;
	
	@Override
	public void onEnable() {
		saveDefaultConfig();
		instance = this;
		kronus = KronusCraft.getInstance();
		invManager = new InventoryManager(this);
		shopManager = new ShopManager();
		voteParty = new VoteParty();
		guildsAPI = new GuildsAPI();
		
		Bukkit.getPluginManager().registerEvents(new SurvivalListener(), this);

		getCommand("help").setExecutor(new HelpCommand());
		getCommand("shops").setExecutor(new ShopsCommand());
		getCommand("shop").setExecutor(new ShopsCommand());
		getCommand("sell").setExecutor(new ShopsCommand());
		getCommand("rank").setExecutor(new RanksCommand());
		getCommand("rankup").setExecutor(new RankupCommand());
		getCommand("ranks").setExecutor(new RankupCommand());
		getCommand("mine").setExecutor(new MineCommand());
		getCommand("credits").setExecutor(new CreditsCommand());
		getCommand("machines").setExecutor(new MachinesCommand());

		if(!setupEconomy()) {
			log("Economy not set up!");
		}
		setupPlayers();
		setupAnnouncer();
		shopManager.setUpShops();
		
		WorldCreator world = new WorldCreator("build");
		world.type(WorldType.FLAT);
		Bukkit.getServer().createWorld(world);
	}

	private void setupAnnouncer() {
		Announcer announce = new Announcer(360);
		Message donate = new Message("&fInterested in helping development of the Network?",
				"&fUse &e/buy &fand browse our &aRanks&f,&a Commands &f& &aPerks!");
		Message one = new Message("&fVote for us now, type &a/vote &fin chat for rewards!");
		Message two = new Message("&fNeed help? At any point use &a/help",
				"&fUseful information, and quick access available!",
				"&fUse &a/helpop <message> &fto talk to a member of staff");
		Message three = new Message("&fRanks are available with extra perks!", "&fUse &a/ranks&f in chat now!");
		Message four = new Message("&fStuck for money? Use &a/shop &fin chat.",
				"&fBuy and sell a variety of items & blocks!");
		Message five = new Message("&fApply for &7[Member]&f rank now! Use &a/member&f in chat!");
		Message six = new Message("&fSupport the development! Use &a/buy&f in chat,",
				"&fBrowse &cRanks&f, &cCosmetics &fand &cPerks&f!");
		Message seven = new Message("&fNeed a place to sell items not in the &a/shop&f?",
				"&fUse the &cAuction House&f! Type &a/ah &fin chat!");
		Message eight = new Message("&fHave spare &cVote Credits&f? Use &a/credits&f in chat now!", "&fPurchase &cKits&f, &cCrate Keys&f, &cCommands &fand &cCurrency");
		Message nine = new Message("&fJoin a vast variety of &cJobs&f!", "&fUse &a/jobs browse&f in chat now!", "&fJobs offer extra &c$$&f and &cPerks&f!");
		Message ten = new Message("&fIn need of easy, quick money?", "&fUse &a/mine &f in chat now!", "&fMine ores and sell them in the &cShop&f!");
		announce.addMessage(donate);
		announce.addMessage(one);
		announce.addMessage(two);
		announce.addMessage(three);
		announce.addMessage(four);
		announce.addMessage(five);
		announce.addMessage(six);
		announce.addMessage(seven);
		announce.addMessage(eight);
		announce.addMessage(nine);
		announce.addMessage(ten);
		announce.start();
	}

	public static Survival getInstance() {
		return instance;
	}

	public Economy getEconomy() {
		return economy;
	}

	public InventoryManager getInvManager() {
		return invManager;
	}
	
	public ShopManager getShopManager() {
		return shopManager;
	}

	public VoteParty getVoteParty() {
		return voteParty;
	}
	
	public GuildsAPI getGuildsAPI() {
		return guildsAPI;
	}
	
	private boolean setupEconomy() {
		RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager()
				.getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}
		return (economy != null);
	}

	private void setupPlayers() {
		for (Player players : Bukkit.getOnlinePlayers()) {
			SurvivalPlayer player = new SurvivalPlayer(players.getUniqueId());
			SurvivalPlayerManager.addPlayer(player);
		}
	}
	
	private void log(String message) {
		System.out.println("[Survival] " + message);
	}
}
