package com.kronuscraftmc.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.kronuscraftmc.SurvivalPlayer;
import com.kronuscraftmc.SurvivalPlayerManager;

public class MineCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		Player player = (Player) s;
		SurvivalPlayer sPlayer = SurvivalPlayerManager.getPlayer(player.getUniqueId());
		player.performCommand("warp mine");
		if (args.length == 0) {
			switch (sPlayer.getRank()) {
			case TOURIST:
				player.performCommand("warp mine");
				break;
			case SETTLER:
				player.performCommand("warp mine");
				break;
			case RESIDENT:
				player.performCommand("warp residentmine");
				break;
			case LANDLORD:
				player.performCommand("warp landlordmine");
				break;
			case MAYOR:
				player.performCommand("warp mayormine");
				break;
			case MONARCH:
				player.performCommand("warp monarchmine");
				break;
			case TYCOON:
			case EMPEROR:
			case LEGEND:
			case IMMORTAL:
			case GODLY:
				player.performCommand("warp tycoonmine");
				break;
			}
		} else if(args.length == 1) {
			player.performCommand("warp " + args[0] + "mine");
		}
		return true;
	}
}
