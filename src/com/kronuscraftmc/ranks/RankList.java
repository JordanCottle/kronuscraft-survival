package com.kronuscraftmc.ranks;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import org.bukkit.ChatColor;

public enum RankList {
	TOURIST(1, "Tourist", ChatColor.DARK_GRAY, 0.0),
	SETTLER(2, "Settler", ChatColor.GRAY, 1000.0),
	RESIDENT(3, "Resident", ChatColor.GRAY, 5000.0),
	LANDLORD(4, "Landlord", ChatColor.WHITE, 15000.0),
	MAYOR(5, "Mayor", ChatColor.AQUA, 40000.0),
	MONARCH(6, "Monarch", ChatColor.BLUE, 100000.0),
	TYCOON(7, "Tycoon", ChatColor.GREEN, 250000.0),
	EMPEROR(8, "Emperor", ChatColor.GOLD, 1000000.0),
	LEGEND(9, "Legend", ChatColor.YELLOW, 2000000.0),
	IMMORTAL(10, "Immortal", ChatColor.RED, 3000000.0),
	GODLY(11, "Godly", ChatColor.DARK_RED, 5000000.0);
	
	private int id;
	private String tag;
	private ChatColor color;
	private double cost;

	private RankList(int id, String tag, ChatColor color, double cost) {
		this.id = id;
		this.tag = tag;
		this.color = color;
		this.cost = cost;
	}

	public int getId() {
		return id;
	}

	public String getTag() {
		return tag;
	}

	public ChatColor getChatColor() {
		return color;
	}
	
	public double getCost() {
		return cost;
	}
	
	public String getCostTag() {
		DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
		symbols.setGroupingSeparator(',');
		formatter.setDecimalFormatSymbols(symbols);	
		return formatter.format(cost);
	}
	
	public static RankList getPlayerRank(int i) {
		for (RankList ranks : RankList.values()) {
			if (i == ranks.getId()) {
				return ranks;
			}
		}
		return null;
	}
	
	public static RankList getPlayerRankByCost(double cost) {
		for (RankList ranks : RankList.values()) {
			if (cost == ranks.getCost()) {
				return ranks;
			}
		}
		return null;
	}
	
	public String getPrefix(boolean bold, boolean uppercase) {
		String name = tag;

		if (uppercase) {
			name = tag.toUpperCase();
		}
		if (bold) {
			name = color + "" + ChatColor.BOLD + name;
		}
		return name;
	}

	public String getTag(boolean bold, boolean uppercase, ChatColor text) {
		String name = text + tag;

		if (!tag.isEmpty()) {
			name = text + "[" + tag + "] " + text;
		}
		if (uppercase) {
			name = tag.toUpperCase();
		}
		if (bold) {
			name = color + "" + ChatColor.BOLD + name + text;
		}

		return name;
	}
}
