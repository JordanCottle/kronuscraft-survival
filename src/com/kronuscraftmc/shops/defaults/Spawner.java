package com.kronuscraftmc.shops.defaults;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;

public enum Spawner {
	// color code spawner buy: &e
	// color code spawner cost: &a&o
	// color code item name: &a
	
	CREEPER ("Creeper Spawner", EntityType.CREEPER, 30000),
	SKELETON ("Skeleton Spawner", EntityType.SKELETON, 25000),
	ZOMBIE ("Zombie Spawner", EntityType.ZOMBIE, 20000),
	SPIDER ("Spider Spawner", EntityType.SPIDER, 22500),
	CAVE_SPIDER ("Cave Spider Spawner", EntityType.CAVE_SPIDER, 25500),
	BLAZE ("Blaze Spawner", EntityType.BLAZE, 125000),
	PIG_ZOMBIE ("Pig Zombie Spawner", EntityType.PIG_ZOMBIE, 40000),
	PIG ("Pig Spawner", EntityType.PIG, 15000),
	COW ("Cow Spawner", EntityType.COW, 15000),
	SHEEP ("Sheep Spawner", EntityType.SHEEP, 15000),
	CHICKEN ("Chicken Spawner", EntityType.CHICKEN, 15000),
	MOOSHROOM ("Mushroom Cow Spawner", EntityType.CHICKEN, 25000),
	HORSE ("Horse Spawner", EntityType.CHICKEN, 100000),
	RABBIT ("Rabbit Spawner", EntityType.RABBIT, 10000);
	
	public final String name;
	public final EntityType entity;
	public final double buyPrice;
	public final double sellPrice;
	Spawner(String name, EntityType entity,  int buyPrice){
		this.name = name;
		this.entity = entity;
		this.buyPrice = buyPrice;
		this.sellPrice = 0;
	}
	
	public static void setUp(FileConfiguration fileConfig) {
		for(Spawner item: Spawner.values()) {
			fileConfig.addDefault(item.name + ".BuyPrice", item.buyPrice);
			fileConfig.addDefault(item.name + ".SellPrice", item.sellPrice);
		}
	}
}
