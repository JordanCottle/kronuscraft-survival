package com.kronuscraftmc.inventorys;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.SurvivalPlayer;
import com.kronuscraftmc.SurvivalPlayerManager;
import com.kronuscraftmc.ranks.RankList;
import com.kronuscraftmc.shops.BaseInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class RankupInventory  extends BaseInventory {
	private static final int ROW = 0;
	private static final int COLUMN = 1;
	enum Shop {
		SETTLER (
				"Settler",
				new String [] {"&f + 1 /sethome","&f + /tpa", "&f + /tpahere", "&f + Silk Touch Spawners",
						 "&f + 250 Bonus Claim Blocks", "&f + /kit settler",
						 /*"&7    - Diamond Sword (Sharpness I)", "&7    - Diamond Pickaxe (Efficiency I)", "&7    - Diamond Axe (Efficiency I)",
						 "&7    - Diamond Shovel (Efficiency I)", "&7    - Diamond Helmet (Protection I)",
						 "&7    - Diamond Chestplate (Protection I)", "&7    - Diamond Leggings (Protection I)",
						 "&7    - Diamond Boots (Protection I)", "&7    - Bread x32", "&7    - XP Bottle x16", 
						 "&7    - Coal x48", "&7    - Iron Ingot x32", "&7    - Gold Ingot x8"*/},
				RankList.SETTLER.getCost(),
				Material.FLINT,
				new int[] {1,2},
				RankList.SETTLER
			),
		RESIDENT (
				"Resident",
				new String [] {"&f + 2 /sethome","&f + /tpa", "&f + /tpahere", 
						"&f + /recipe", "&f + Silk Touch Spawners", "&f + Join upto 2 Jobs",
						"&f + 250 Bonus Claim Blocks", "&f + /mine resident"},
				RankList.RESIDENT.getCost(),
				Material.QUARTZ,
				new int[] {1,3},
				RankList.RESIDENT
		),	
		LANDLORD (
				"Landlord",
				new String [] {"&f + 3 /sethome","&f + /tpa", "&f  + /tpahere","&f + /recipe", 
						"&f + /afk", "&f + 250 Bonus Claim Blocks", "&f + /mine landlord", "&f + /kit landlord",
						/*"&7    - Diamond Sword (Sharpness II Knockback I)", "&7    - Diamond Pickaxe (Efficiency II Unbreaking I)",
						"&7    - Diamond Axe (Efficiency II Unbreaking I)", "&7    - Diamond Shovel (Efficiency II Unbreaking I)",
						"&7    - Diamond Helmet (Protection II)", "&7    - Diamond Chestplate (Protection II)", 
						"&7    - Diamond Leggings (Protection II)", "&7    - Diamond Boots (Protection II)",
						"&7    - Bread x64", "&7    - XP Bottle x32", "&7    - God Apple x4", "&7    - Coal x64",
						"&7    - Iron Ingot x48", "&7    - Gold Ingot x16", "&7    - Diamond x8"*/},
				RankList.LANDLORD.getCost(),
				Material.COAL,
				new int[] {1,4},
				RankList.LANDLORD
		),
		MAYOR (
				"Mayor",
				new String [] {"&f + 4 /sethome","&f + /tpa", "&f + /tpahere", "&f + /recipe",
						"&f + /afk", "&f + /pweather <sun/rain>", "&f + /ptime <day/night>",
						"&f + 250 Bonus Claim Blocks", "&f + /mine mayor"},
				RankList.MAYOR.getCost(),
				Material.IRON_INGOT,
				new int[] {1,5},
				RankList.MAYOR
		),
		MONARCH (
				"Monarch",
				new String [] {"&f + 4 /sethome","&f + /tpa", "&f + /tpahere", "&f + /recipe",
						"&f + /afk", "&f + /pweather <sun/rain>", "&f + /ptime <day/night>",
						"&f + Join upto 3 Jobs", "&f + 250 Bonus Claim Blocks", "&f + /mine monarch", "&f + /kit monarch",
						/*"&7    - Diamond Sword (Sharpness III Unbreaking I Knockback I)", "&7    - Diamond Pickaxe (Efficiency III Fortune I Unbreaking I)",
						"&7    - Diamond Axe (Efficiency III Unbreaking I)", "&7    - Diamond Shovel (Efficiency III Unbreaking I)",
						"&7    - Diamond Helmet (Protection III)", "&7    - Diamond Chestplate (Protection III)", 
						"&7    - Diamond Leggings (Protection III)", "&7    - Diamond Boots (Protection III)",
						"&7    - Bread x64", "&7    - XP Bottle x48", "&7    - God Apple x8", "&7    - Coal x128",
						"&7    - Iron Ingot x64", "&7    - Gold Ingot x32", "&7    - Diamond x10"*/},
				RankList.MONARCH.getCost(),
				Material.REDSTONE,
				new int[] {1,6},
				RankList.MONARCH
		),
		TYCOON (
				"Tycoon",
				new String [] {"&f + 5 /sethome","&f + /tpa", "&f + /tpahere", "&f + /recipe",
						"&f + /afk", "&f + /pweather <sun/rain>", "&f + /ptime <day/night>", "&f + /workbench",
						"&f + Join upto 4 Jobs", "&f + 500 Bonus Claim Blocks", "&f + /mine tycoon", "&f + /kit tycoon",
					/*	"&7    - Diamond Sword (Sharpness III Unbreaking II Knockback II)", "&7    - Diamond Pickaxe (Efficiency III Fortune II Unbreaking II)",
						"&7    - Diamond Axe (Efficiency III Unbreaking II)", "&7    - Diamond Shovel (Efficiency III Unbreaking II)",
						"&7    - Diamond Helmet (Protection III Fire Protection I Respiration I)", "&7    - Diamond Chestplate (Protection III Fire Protection I)", 
						"&7    - Diamond Leggings (Protection III Fire Protection I)", "&7    - Diamond Boots (Protection III Fire Protection I)",
						"&7    - Bread x64", "&7    - XP Bottle x64", "&7    - God Apple x16", "&7    - Coal x128",
						"&7    - Iron Ingot x64", "&7    - Gold Ingot x32", "&7    - Diamond x16"*/},
				RankList.TYCOON.getCost(),
				Material.LAPIS_LAZULI,
				new int[] {2,2},
				RankList.TYCOON
		),
		EMPEROR (
				"Emperor",
				new String[] {"&f + 6 /sethome","&f + /tpa", "&f + /tpahere", "&f + /recipe",
						"&f + /afk", "&f + /pweather <sun/rain>", "&f + /ptime <day/night>", "&f + /workbench", "&f + /enderchest",
						"&f + Join upto 4 Jobs", "&f + 500 Bonus Claim Blocks", "&f + /kit emperor",
						/*"&7    - Diamond Sword (Sharpness III Fire I Knockback II Unbreaking III)", "&7    - Diamond Pickaxe (Efficiency IV Fortune III Unbreaking III)",
						"&7    - Diamond Axe (Efficiency IV Unbreaking III)", "&7    - Diamond Shovel (Efficiency IV Unbreaking III)",
						"&7    - Diamond Helmet (Protection IV Fire Protection II Thorns I Respiration I)", "&7    - Diamond Chestplate (Protection IV Fire Protection II Thorns I)", 
						"&7    - Diamond Leggings (Protection IV Fire Protection II Thorns I)", "&7    - Diamond Boots (Protection IV Fire Protection II Thorns I)",
						"&7    - Bread x64", "&7    - XP Bottle x64", "&7    - God Apple x32", "&7    - Coal x128",
						"&7    - Iron Ingot x128", "&7    - Gold Ingot x64", "&7    - Diamond x32", "&7    - Emerald x12"*/},
				RankList.EMPEROR.getCost(),
				Material.GOLD_INGOT,
				new int[] {2,3},
				RankList.EMPEROR
		),
		LEGEND (
				"Legend",
				new String[] {"&f + 7 /sethome","&f + /tpa", "&f + /tpahere", "&f + /recipe",
						"&f + /afk", "&f + /pweather <sun/rain>", "&f + /ptime <day/night>", "&f + /workbench", "&f + /enderchest", "&f + /back",
						"&f + Join upto 5 Jobs", "&f + 500 Bonus Claim Blocks", "&f + /kit legend",
					/*	"&7    - Diamond Sword (Sharpness IV Fire I Knockback II Unbreaking III)", "&7    - Diamond Pickaxe (Efficiency V Fortune III Unbreaking III)",
						"&7    - Diamond Axe (Efficiency V Unbreaking III)", "&7    - Diamond Shovel (Efficiency V Unbreaking III)",
						"&7    - Diamond Helmet (Protection IV Fire Protection III Thorns I Respiration I)", "&7    - Diamond Chestplate (Protection IV Fire Protection III Thorns I)", 
						"&7    - Diamond Leggings (Protection IV Fire Protection III Thorns I)", "&7    - Diamond Boots (Protection IV Fire Protection III Thorns I)",
						"&7    - Bread x64", "&7    - XP Bottle x64", "&7    - God Apple x32", "&7    - Coal x128",
						"&7    - Iron Ingot x128", "&7    - Gold Ingot x96", "&7    - Diamond x64", "&7    - Emerald x32"*/},
				RankList.LEGEND.getCost(),
				Material.DIAMOND,
				new int[] {2,4},
				RankList.LEGEND
		),
		IMMORTAL (
				"Immortal",
				new String[] {"&f + 8 /sethome","&f + /tpa", "&f + /tpahere", "&f + /recipe",
						"&f + /afk", "&f + /pweather <sun/rain>", "&f + /ptime <day/night>", "&f + /workbench", "&f + /enderchest", "&f + /back", "&f + /feed",
						"&f + Join upto 5 Jobs", "&f + 500 Bonus Claim Blocks", "&f + /kit immortal",
						/*"&7    - Diamond Sword (Sharpness IV Fire I Knockback II Mending I Unbreaking III)", "&7    - Diamond Pickaxe (Efficiency V Fortune III Unbreaking III)",
						"&7    - Diamond Axe (Efficiency V Unbreaking III)", "&7    - Diamond Shovel (Efficiency V Unbreaking III)",
						"&7    - Diamond Helmet (Protection IV Fire Protection III Thorns I Respiration I)", "&7    - Diamond Chestplate (Protection IV  Fire Protection III Thorns I)", 
						"&7    - Diamond Leggings (Protection IV Fire Protection III Thorns I)", "&7    - Diamond Boots (Protection IV Fire Protection III Thorns I)",
						"&7    - Bread x64", "&7    - XP Bottle x64", "&7    - God Apple x32", "&7    - Coal x128",
						"&7    - Iron Ingot x128", "&7    - Gold Ingot x128", "&7    - Diamond x96", "&7    - Emerald x64"*/},
				RankList.IMMORTAL.getCost(),
				Material.EMERALD,
				new int[] {2,5},
				RankList.IMMORTAL
		),
		GODLY (
				"Godly",
				new String[] {"&f + 9 /sethome","&f + /tpa", "&f + /tpahere", "&f + /recipe",
						"&f + /afk", "&f + /pweather <sun/rain>", "&f + /ptime <day/night>", "&f + /workbench", "&f + /enderchest", "&f + /back", "&f + /feed", "&f + /heal",
						"&f + Join upto 6 Jobs", "&f + 500 Bonus Claim Blocks", "&f + /kit godly",
						/*"&7    - Diamond Sword (Sharpness IV Fire II Knockback II Mending I Unbreaking III)", "&7    - Diamond Pickaxe (Efficiency V Fortune III Unbreaking III)",
						"&7    - Diamond Axe (Efficiency V Unbreaking III)", "&7    - Diamond Shovel (Efficiency V Unbreaking III)",
						"&7    - Diamond Helmet (Protection IV Fire Protection III Thorns II Respiration I)", "&7    - Diamond Chestplate (Protection IV Fire Protection III Thorns II)", 
						"&7    - Diamond Leggings (Protection IV Fire Protection III Thorns II)", "&7    - Diamond Boots (Protection IV Fire Protection III Thorns II)",
						"&7    - Bread x64", "&7    - XP Bottle x64", "&7    - God Apple x32", "&7    - Coal x128",
						"&7    - Iron Ingot x128", "&7    - Gold Ingot x128", "&7    - Diamond x128", "&7    - Emerald x96"*/},
				RankList.GODLY.getCost(),
				Material.BEDROCK,
				new int[] {2,6},
				RankList.GODLY
		);

		public final String name;
		public final String[] loreLines;
		public final double cost;
		public final Material guiObject;
		public final int[] containerCoordinates;
		public final RankList rank;
		Shop(String title, String[] loreLines, double cost, Material guiObject, int[] containerCoordinates, RankList rank){
			this.name = rank.getChatColor() + "" + rank.getTag(true, false, rank.getChatColor()) + "Rank";
			this.loreLines = loreLines;
			this.cost = cost;
			this.guiObject = guiObject;
			this.containerCoordinates = containerCoordinates;
			this.rank = rank;
		}

	}
	
	public static final SmartInventory INVENTORY = SmartInventory.builder().id("ranks").provider(new RankupInventory())
			.size(5, 9).title(ChatColor.GREEN + "Ranks GUI").build();

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		
		SurvivalPlayer sPlayer = SurvivalPlayerManager.getPlayer(player.getName());
		double money = sPlayer.getMoney();

		for(Shop shop : Shop.values()) {
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.translateAlternateColorCodes('&', "&8&oCost: &b$" + shop.rank.getCostTag()));
			lore.add(ChatColor.translateAlternateColorCodes('&', "&7Perks:"));
			for(String loresLines : shop.loreLines) {
				lore.add(ChatColor.translateAlternateColorCodes('&', loresLines));
			}
			ItemStack item = createItem(shop.guiObject, shop.name, lore);
			ClickableItem icon = ClickableItem.of(item, e -> {
				player.closeInventory();
				rankupPlayer(sPlayer, shop.rank, money);
			});
			cont.set(shop.containerCoordinates[ROW], shop.containerCoordinates[COLUMN], icon);
		}
		ItemStack gui = createItem(Material.BARRIER, ChatColor.RED + "Back to Help GUI");
		cont.set(3, 4, ClickableItem.of(gui, e -> HelpInventory.INVENTORY.open(player)));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {
	
	}
}
