package com.kronuscraftmc.shops;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.SurvivalPlayer;
import com.kronuscraftmc.ranks.RankList;

import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryProvider;
import net.milkbowl.vault.economy.Economy;

/**
 * Contains common functions and information required by shops
 * @author Jordan Cottle
 *
 */
public abstract class BaseInventory implements InventoryProvider {
	public SmartInventory inventory;
	public ItemStack icon;
	public int[] navCoordinates;
	protected static final String NAV_HEADING = "&8&oBuy and Sell";
	protected static final String SHOP_DESCRIPTION_COLOR = "&7";
	protected static final String SHOP_NAME_COLOR = "&a";
	protected static final String SHOP_LOG_TAG = "[ShopInventory] ";
	protected static final String TRANSACTION_FAIL_COLOR = "&4";
	protected static final String TRANSACTION_SUCCESS_COLOR = "&a";
	
	protected static Economy economy = Survival.getInstance().getEconomy();
	private static boolean testing = false;
	
	protected static boolean rankupPlayer(SurvivalPlayer sPlayer, RankList rank, double money) {
		Player player = sPlayer.getPlayer();
		player.getPlayer().closeInventory();
		if(sPlayer.getRank() == rank || sPlayer.getRank().getId() > rank.getId()) {
			player.sendMessage(ChatColor.RED + "You've already purchased this rank!");
			return false;
		} else if((sPlayer.getRank().getId() + 1) != rank.getId()) {
			player.sendMessage(ChatColor.RED + "You can't skip ranks!");
			return false;
		} else if(money >= rank.getCost()) {
			sPlayer.setRank(rank);
			sPlayer.removeMoney(rank.getCost());
			player.sendMessage(ChatColor.GRAY + "You're now rank " + rank.getTag(true, false, rank.getChatColor()));
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "pex user " + player.getName() + " group add " + rank.getTag());
			
			switch(rank) {
			case TYCOON:
			case EMPEROR:
			case LEGEND:
			case IMMORTAL:
			case GODLY:
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "acb " + player.getName() + " 500");
				default:
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "acb " + player.getName() + " 250");
					
			}
			return true;
		} else {
			player.sendMessage(ChatColor.GRAY + "You don't have enough $ for that!");
			return false;
		}
	}
	
	/**
	 *  Sells a spawner to a player
	 * @param player The player to sell to
	 * @param cost	The cost of the spawner
	 * @param spawnerName The name of the spawner to sell
	 * @return Whether or not the sale was successful
	 */
	protected static boolean sellItem(Player player, double cost, String spawnerName) {
		if(!checkForEmptyInventorySpace(player.getInventory())) {
			message(player, TRANSACTION_FAIL_COLOR, "You need a space to put that!");
			return false;
		}
		
		if(testing || economy.getBalance(player) >= cost) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spawnergive " + player.getName() + " " + spawnerName +" 1");
			if (!testing) economy.withdrawPlayer(player, cost);
			message(player, TRANSACTION_SUCCESS_COLOR, "Purchase completed!");
			return true;
		}
		message(player, TRANSACTION_FAIL_COLOR, "You need more $ for that!");
		return false;
	}
	
	/**
	 * Sells an enchanted book to a player
	 * 
	 * @param player The player to sell to
	 * @param cost The cost of the item being sold
	 * @param enchant The enchantment to apply to the book
	 * @param level	The level of the enchantment applied to the book
	 * @return Whether or not the transaction was successful
	 */
	protected static boolean sellItem(Player player, double cost, Enchantment enchant, int level) {
		if(!checkForEmptyInventorySpace(player.getInventory())) {
			message(player, TRANSACTION_FAIL_COLOR, "You need a space to put that!");
			return false;
		}
		
		if(testing || Survival.getInstance().getEconomy().getBalance(player) >= cost) {
			player.getInventory().addItem(addBookEnchantment(enchant, level));
			if (!testing) economy.withdrawPlayer(player, cost);
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&aKC&8] &7Purchase completed!"));
			return true;
		}
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&aKC&8] You need more $ for that!"));
		return false;
	}
	
	/**
	 * Sells an ItemStack to a player
	 * 
	 * @param player The player to sell to
	 * @param cost	The cost per item
	 * @param item	The ItemStack to sell to the player
	 * @param buyAmount The number of the given ItemStack to sell to the player
	 * @return Whether or not the transaction was successful
	 */
	protected static boolean sellItem(Player player, double cost, ItemStack item, int buyAmount) {
		if(!checkForEmptyInventorySpace(player.getInventory())) {
			message(player, TRANSACTION_FAIL_COLOR, "You need a space to put that!");
			return false;
		}
		
		Inventory playerInventory = player.getInventory();
		if(cost != 0) {
			if(testing || economy.getBalance(player) >= cost) {
				for(int i = 0; i < buyAmount; i++) {
					playerInventory.addItem(item);
					if(!testing) economy.withdrawPlayer(player, cost);
				}

				String itemName = getFriendlyName(item);
				message(player, TRANSACTION_SUCCESS_COLOR, String.format("Bought %d x %s for %.2f", 
						buyAmount, 
						itemName, 
						cost * buyAmount));
				return true;
			}
			else {
				message(player, TRANSACTION_FAIL_COLOR, "You need more $ for that!");
				return false;
			}
		}
		return false;
	}

	/**
	 * Sells an ItemStack to a player
	 * 
	 * @param player  The player to sell to
	 * @param buyableItem The item to sell
	 * @param buyAmount The count of the item to sell
	 * @return Whether or not the sale was successful
	 */
	protected static boolean sellItem(Player player, BuyableItem buyableItem, int buyAmount) {
		if(!checkForEmptyInventorySpace(player.getInventory())) {
			message(player, TRANSACTION_FAIL_COLOR, "You need a space to put that!");
			return false;
		}
		
		Inventory playerInventory = player.getInventory();
		ItemStack item = buyableItem.getItem(buyAmount);
		double cost = buyableItem.getBuyPrice(buyAmount);
		
		if(cost != 0) {
			if (testing || economy.getBalance(player) >= cost) {
				playerInventory.addItem(item);
				if (!testing)
					economy.withdrawPlayer(player, cost);

				String itemName = getFriendlyName(item);
				message(player, TRANSACTION_SUCCESS_COLOR,
						String.format("Bought %d x %s for %.2f", buyAmount, itemName, cost));
				return true;
			} else {
				message(player, TRANSACTION_FAIL_COLOR, "You need more $ for that!");
				return false;
			}
		}
		return false;
	}

	/**
	 * Buys an ItemStack from a player
	 * 
	 * @param player The player to buy from
	 * @param item The BuyableItem that contains the information about the item to purchase
	 * @param sellAmount The amount of the specified item to purchase from the player
	 * @return Whether the purchase from the player was successful or not
	 */
	protected static boolean buyItem(Player player, BuyableItem buyableItem, int sellAmount){
		Inventory playerInventory = player.getInventory();

		ItemStack item = buyableItem.getItem(sellAmount);
		double cost = buyableItem.getSellPrice(sellAmount);
		
		String itemName = getFriendlyName(item);
		if (playerInventory.containsAtLeast(item, sellAmount)) {
			playerInventory.removeItem(item);
			if (!testing)
				economy.depositPlayer(player, cost);

			message(player, TRANSACTION_SUCCESS_COLOR,
					String.format("Sold %d x %s for %.2f", sellAmount, itemName, cost));
			log(player.getCustomName() + String.format(" Sold %d x %s for %.2f", sellAmount, itemName, cost));
			return true;
		} else {
			message(player, TRANSACTION_FAIL_COLOR, "You don't have enough " + itemName);
			return false;
		}
	}
	
	protected static boolean buyAll(Player player, BuyableItem buyableItem) {
		Inventory playerInventory = player.getInventory();

		int sellAmount = 0;
		ItemStack item = buyableItem.getItem(sellAmount);

		// count whole stacks at a time
		int delta = 64;
		while(playerInventory.containsAtLeast(item, sellAmount+delta)) {
			sellAmount += delta;
		}
		
		// count any extra items 1 at a time
		delta = 1;
		while(playerInventory.containsAtLeast(item, sellAmount+delta)) {
			sellAmount += delta;
		}
		
		if(sellAmount == 0) {
			message(player, TRANSACTION_FAIL_COLOR, "You don't have any " + getFriendlyName(item));
			return false;
		}
		
		return buyItem(player, buyableItem, sellAmount);
	}
	
	/**
	 * Creates a custom spawner item
	 * 
	 * @param entity The entity the spawner should spawn
	 * @param name	The desired display name of the item
	 * @param lores The description of the spawner
	 * @return The spawner with specified display name and description as an ItemStack
	 */
	protected static ItemStack createItem(EntityType entity, String name, List<String> lores) {
		ItemStack item = new ItemStack(Material.SPAWNER, 1);
        BlockStateMeta meta = (BlockStateMeta) item.getItemMeta();
        meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        meta.setLore(lores);
        CreatureSpawner creatureSpawner = (CreatureSpawner) meta.getBlockState();
        creatureSpawner.setSpawnedType(entity);
        meta.setBlockState(creatureSpawner);
        item.setItemMeta(meta);
		return item;
	}
	
	/**
	 * Creates an item with a customized name and description
	 * 
	 * @param material The material of the desired ItemStack
	 * @param name	The custom name for the ItemStack
	 * @param lores	The description to add to the ItemStack
	 * @return	The ItemStack with desired name and description
	 */
	protected static ItemStack createItem(Material material, String name, List<String> lores) {
		ItemStack item = new ItemStack(material);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		meta.setLore(lores);
		item.setItemMeta(meta);
		return item;
	}
	
	/**
	 * Creates a new ItemStack with a customized name
	 * 
	 * @param material The material of the desired ItemStack
	 * @param name	The custom name for the ItemStack
	 * @return	The ItemStack with desired name
	 */
	protected static ItemStack createItem(Material material, String name) {
		ItemStack item = new ItemStack(material);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		item.setItemMeta(meta);
		return item;
	}
	
	/**
	 * Creates a new ItemStack with a customized name and given amount
	 * 
	 * @param material The material to use for the new item
	 * @param name The description/lore of the item to be created
	 * @param amount The amount of items to be contained in the stack
	 * @return The ItemStack of the specified material with given name and amount
	 */
	protected static ItemStack createItem(Material material, String name, int amount) {
		ItemStack item = new ItemStack(material, amount);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		item.setItemMeta(meta);
		return item;
	}

	/**
	 * Creates a new ItemStack with a customized description and amount
	 * 
	 * @param material The material to use for the new item
	 * @param lores The description/lore of the item to be created
	 * @param amount The amount of items to be contained in the stack
	 * @return The ItemStack of the specified material with given lore attached to it
	 */
	protected static ItemStack createItem(Material material, List<String> lores, int amount) {
		ItemStack item = new ItemStack(material, amount);
		ItemMeta meta = item.getItemMeta();
		meta.setLore(lores);
		item.setItemMeta(meta);
		return item;
	}

	/**
	 * Creates a lore list from a color code, heading, and list of strings
	 * 
	 * @param colorCode String that contains just the formatting code for each line, except the heading
	 * @param heading String that appear on the top line of the item's description
	 * @param lines  A dynamic String array that represent the text to place in the lore
	 * 
	 * @return returns the completed lore list with properly formatted text
	 */
	protected static List<String> createLore(String colorCode, String heading, String... lines){
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.translateAlternateColorCodes('&', heading));

		for (String line: lines){
			lore.add(ChatColor.translateAlternateColorCodes('&', colorCode + line));
		}

		return lore;
	}
	
	/**
	 * Creates a lore list from a color code and list of strings
	 * 
	 * @param colorCode String that contains just the formatting code for each line
	 * @param lines  A dynamic String array that represent the text to place in the lore
	 * 
	 * @return returns the completed lore list with properly formatted text
	 */
	protected static List<String> createLore(String colorCode, String... lines){
		List<String> lore = new ArrayList<String>();

		for (String line: lines){
			lore.add(ChatColor.translateAlternateColorCodes('&', colorCode + line));
		}

		return lore;
	}
	
	/**
	 * Creates an enchanted book
	 * 
	 * @param enchantment The enchantment to place on the book
	 * @param level	The level of the enchantment to place on the book
	 * @return The ItemStack that represents the desired enchanted book
	 */
	protected static ItemStack addBookEnchantment(Enchantment enchantment, int level) {
		ItemStack item = new ItemStack(Material.ENCHANTED_BOOK);
		EnchantmentStorageMeta meta = (EnchantmentStorageMeta) item.getItemMeta();
		meta.addStoredEnchant(enchantment, level, true);
		item.setItemMeta(meta);
		return item;
	}
	
	/**
	 * Sends the message to the console with a header to identify which class generated it
	 * 
	 * @param message The message to log
	 */
	protected static void log(String message) {
		System.out.println(SHOP_LOG_TAG + message);
	}
	
	/**
	 * Sends a message to the player with the specified color for flavor
	 * 
	 * @param message
	 */
	protected static void message(Player player, String colorCode, String message) {
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', colorCode + message));
	}
	
	protected static String getFriendlyName(ItemStack item) {
		String badName;
		if (item.getItemMeta().hasDisplayName()) {
			badName = item.getItemMeta().getDisplayName();
		} else {
			badName = item.getType().toString();
		}
		
		StringBuilder sb = new StringBuilder();
		for (String str : badName.split("_"))
		sb.append(" ").append(Character.toUpperCase(str.charAt(0))).append(str.substring(1).toLowerCase());
		return sb.toString().trim().replace("Diode", "Redstone Repeater").replace("Thin Glass", "Glass Pane").replace("Wood ", "Wooden ");
	}
	
	protected static boolean checkForEmptyInventorySpace(Inventory inventory) {
		return inventory.firstEmpty() != -1;
	}
}
