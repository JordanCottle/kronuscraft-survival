package com.kronuscraftmc.inventorys.credits;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.inventorys.HelpInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;

public class CreditMasterInventory implements InventoryProvider {

	public static final SmartInventory INVENTORY = SmartInventory.builder().id("credit").provider(new CreditMasterInventory())
			.size(5, 9).title(ChatColor.AQUA + "Credit Shop").build();

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		
		List<String> lore0 = new ArrayList<String>();
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&fPurchase never expiring kits!"));
		ItemStack item0 = createItem(Material.DIAMOND_HELMET, "&aKits", lore0);
		
		List<String> lore1 = new ArrayList<String>();
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&fPurchase usable commands!"));
		ItemStack item1 = createItem(Material.STICK, "&aCommands", lore1);
		
		List<String> lore2 = new ArrayList<String>();
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&fPurchase in-game currency!"));
		ItemStack item2 = createItem(Material.PAPER, "&aCurrency", lore2);
		
		List<String> lore3 = new ArrayList<String>();
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&fPurchase Crate Keys here!"));
		ItemStack item3 = createItem(Material.TRIPWIRE_HOOK, "&aCrate Keys", lore3);
		
		List<String> lore4 = new ArrayList<String>();
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&fPurchase Bonus Claim Blocks here!"));
		ItemStack item4 = createItem(Material.GRASS_BLOCK, "&aBonus Claim Blocks", lore4);
		
		ClickableItem item0Click = ClickableItem.of(item0, e -> {
			KitInventory.INVENTORY.open(player);
		});
		ClickableItem item1Click = ClickableItem.of(item1, e -> {
			CommandInventory.INVENTORY.open(player);
		});
		ClickableItem item2Click = ClickableItem.of(item2, e -> {
			CurrencyInventory.INVENTORY.open(player);
		});
		ClickableItem item3Click = ClickableItem.of(item3, e -> {
			CrateKeyInventory.INVENTORY.open(player);
		});
		ClickableItem item4Click = ClickableItem.of(item4, e -> {
			BonusClaimBlocksInventory.INVENTORY.open(player);
		});
		
		ItemStack gui = createItem(Material.BARRIER, ChatColor.RED + "Back to Help GUI");
		cont.set(1, 1, item0Click);
		cont.set(1, 3, item1Click);
		cont.set(1, 5, item2Click);
		cont.set(1, 7, item3Click);
		cont.set(2, 4, item4Click);
		cont.set(3, 7, ClickableItem.of(gui, e -> HelpInventory.INVENTORY.open(player)));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}
	
	private ItemStack createItem(Material m, String name) {
		ItemStack item = new ItemStack(m);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		item.setItemMeta(meta);
		return item;
	}

	private ItemStack createItem(Material m, String name, List<String> lores) {
		ItemStack item = new ItemStack(m);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		meta.setLore(lores);
		item.setItemMeta(meta);
		return item;
	}
}
