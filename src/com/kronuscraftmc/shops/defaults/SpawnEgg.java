package com.kronuscraftmc.shops.defaults;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

public enum SpawnEgg {
	// color code egg buy: &e&o
	// color code egg cost: &a
	//color code item name: &a
	VILLAGER_SPAWN_EGG ("Villager Egg", Material.VILLAGER_SPAWN_EGG, 500),
	PIG_SPAWN_EGG ("Pig Egg", Material.PIG_SPAWN_EGG, 500),
	SHEEP_SPAWN_EGG ("Sheep Egg", Material.SHEEP_SPAWN_EGG, 400),
	COW_SPAWN_EGG ("Cow Egg", Material.COW_SPAWN_EGG, 400),
	CHICKEN_SPAWN_EGG ("Chicken Egg", Material.CHICKEN_SPAWN_EGG, 400),
	TROPICAL_FISH_SPAWN_EGG ("Tropical Fish Egg", Material.TROPICAL_FISH_SPAWN_EGG, 250),
	RABBIT_SPAWN_EGG ("Rabbit Egg", Material.RABBIT_SPAWN_EGG, 200);
	
	public final String name;
	public final Material material;
	public final double buyPrice;
	public final double sellPrice;
	SpawnEgg(String name, Material material,  int buyPrice){
		this.name = name;
		this.material = material;
		this.buyPrice = buyPrice;
		this.sellPrice = 0;
	}
	
	public static void setUp(FileConfiguration fileConfig) {
		for(SpawnEgg item: SpawnEgg.values()) {
			fileConfig.addDefault(item.name + ".BuyPrice", item.buyPrice);
			fileConfig.addDefault(item.name + ".SellPrice", item.sellPrice);
		}
	}
}
