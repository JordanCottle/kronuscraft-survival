package com.kronuscraftmc.inventorys.donate;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.shops.BaseInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class RanksInventory extends BaseInventory {
	private static final int ROW = 0;
	private static final int COLUMN = 1;
	public static final SmartInventory INVENTORY = SmartInventory.builder().id("dranks").provider(new RanksInventory())
			.size(4, 9).title(ChatColor.RED + "Ranks GUI").build();

	
	enum Shop {
		PREMIUN (
				"&e&lPremium",
				new String [] { "&f + Fancy &2[Premium]&f Tag in-game!", "&f + /back", "&f + /feed",
						"&f + /clearinventory", "&f + /hat", "&f + 50% McMMO Exp Boost", "&f + Keep XP On Death",
						"&f + $25,000 In-Game Money", "&f + 5x Sethomes", "&f + /kit premium"},
				Material.ENCHANTED_BOOK,
				new int[] {1,2} 
				),
		VIP (
				"&e&lVIP",
				new String [] { "&f + Fancy &e[VIP]&f Tag in-game!", "&f + /back", "&f + /feed",
						"&f + /clearinventory", "&f + /hat", "&f + /workbench", "&f + /heal", "&f + /near", 
						"&f + /jump", "&f + 100% McMMO Exp Boost", "&f + Keep XP On Death",
						"&f + $50,000 In-Game Money", "&f + 10x Sethomes", "&f + /kit vip"},
				Material.ENCHANTED_BOOK,
				new int[] {1,4}
				),
		MVP (
				"&6&lMVP",
				new String [] { "&f + Fancy &6[MVP]&f Tag in-game!", "&f + /back", "&f + /feed",
						"&f + /clearinventory", "&f + /hat", "&f + /workbench", "&f + /heal", "&f + /near", 
						"&f + /jump", "&f + /enderchest", "&f + /fix", "&f + /skull <name>", "&f + /bigtree", "&f + Triple McMMO Exp Boost", "&f + Keep XP On Death",
						"&f + $100,000 In-Game Money", "&f + 15x Sethomes", "&f + /kit mvp"},
				Material.ENCHANTED_BOOK,
				new int[] {1,6} 
			),
		Elite (
				"&c&lElite",
				new String [] { "&f + Fancy &c[Elite]&f Tag in-game!", "&f + /back", "&f + /feed",
						"&f + /clearinventory", "&f + /hat", "&f + /workbench", "&f + /heal", "&f + /near", 
						"&f + /jump", "&f + /enderchest", "&f + /fix", "&f + /skull <name>", "&f + /bigtree", "&f + /nick <name>",
						"&f + /enchant <enchantment> <level>", "&f + /nv", "&f + /run", "&f + Quadruple McMMO Exp Boost", "&f + Keep XP On Death",
						"&f + $250,000 In-Game Money", "&f + 20x Sethomes", "&f + /kit elite"},
				Material.ENCHANTED_BOOK,
				new int[] {1,6} 
			);
		
		public final String name;
		public final String[] loreLines;
		public final Material guiObject;
		public final int[] containerCoordinates;
		Shop(String title, String[] loreLines, Material guiObject, int[] containerCoordinates){
			this.name = title;
			this.loreLines = loreLines;
			this.guiObject = guiObject;
			this.containerCoordinates = containerCoordinates;
		}
}

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		for (Shop shop : Shop.values()) {
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.translateAlternateColorCodes('&', "&b&oClick to view link!"));
			lore.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
			for (String loresLines : shop.loreLines) {
				lore.add(ChatColor.translateAlternateColorCodes('&', loresLines));
			}
			ItemStack item = BaseInventory.createItem(shop.guiObject, shop.name, lore);
			ClickableItem icon = ClickableItem.of(item, e -> {
				player.closeInventory();
				player.sendMessage("");
				player.sendMessage("");
				player.sendMessage("");
				player.sendMessage(ChatColor.GRAY + "Click here: " + ChatColor.AQUA + "http://kronuscraft.buycraft.net/category/1255738");
				player.sendMessage(ChatColor.RED + "Type " + ChatColor.AQUA + "/buy" + ChatColor.RED + " in chat to view our Shop!");
			});

			cont.set(shop.containerCoordinates[ROW], shop.containerCoordinates[COLUMN], icon);
		}
		ItemStack gui = BaseInventory.createItem(Material.BARRIER, ChatColor.RED + "Back to Donate GUI");
		cont.set(2, 7, ClickableItem.of(gui, e -> DonateInventory.INVENTORY.open(player)));
	}
	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}
}
