package com.kronuscraftmc.shops.defaults;

import org.bukkit.configuration.file.FileConfiguration;

public enum Farm {
	SHEARS("SHEARS", 50.0, 0.0),
	IRON_HOE("IRON_HOE", 50.0, 0.0),
	DIAMOND_HOE("DIAMOND_HOE", 150.0, 0.0),
	WHEAT_SEEDS("WHEAT_SEEDS", 1.0, 1.0),
	PUMPKIN_SEEDS("PUMPKIN_SEEDS", 3.0, 1.0),
	MELON_SEEDS("MELON_SEEDS", 2.0, 1.0),
	BEETROOT_SEEDS("BEETROOT_SEEDS", 4.0, 1.0),
	COCOA_BEANS("COCOA_BEANS", 5.0, 2.0),
	OAK_SAPLING("OAK_SAPLING", 2.0, 1.0),
	DARK_OAK_SAPLING("DARK_OAK_SAPLING", 2.0, 1.0),
	SPRUCE_SAPPLING("SPRUCE_SAPLING", 2.0, 1.0),
	JUNGLE_SAPLING("JUNGLE_SAPLING", 2.0, 1.0),
	BIRCH_SAPLING("BIRCH_SAPLING", 2.0, 1.0),
	PUMPKIN("PUMPKIN", 6.0, 5.0),
	SUGAR("SUGAR", 2.0, 1.0),
	SUGAR_CANE("SUGAR_CANE", 5.0, 4.0),
	CACTUS("CACTUS", 6.0, 3.0),
	WHEAT("WHEAT", 5.0, 4.0),
	BONE_MEAL("BONE_MEAL", 15.0, 1.0),
	BREAD("BREAD", 10.0, 7.0),
	MELON_SLICE("MELON_SLICE", 3.0, 2.0),
	CARROT("CARROT", 5.0, 4.0),
	POTATO("POTATO", 5.0, 4.0),
	COOKIE("COOKIE", 5.0, 0.0),
	APPLE("APPLE", 3.0, 3.0);
	
	public final String name;
	public final double buyPrice;
	public final double sellPrice;
	Farm(String name, double buyPrice, double sellPrice){
		this.name = name;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
	}
	
	public static void setUp(FileConfiguration fileConfig) {
		for(Farm item: Farm.values()) {
			fileConfig.addDefault(item.name + ".BuyPrice", item.buyPrice);
			fileConfig.addDefault(item.name + ".SellPrice", item.sellPrice);
		}
	}
}
