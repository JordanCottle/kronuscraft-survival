package com.kronuscraftmc.inventorys.credits;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.SurvivalPlayer;
import com.kronuscraftmc.SurvivalPlayerManager;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;

public class CurrencyInventory implements InventoryProvider {

	public static final SmartInventory INVENTORY = SmartInventory.builder().id("currency").provider(new CurrencyInventory())
			.size(4, 9).title(ChatColor.GREEN + "Currency Converter").build();

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		
		List<String> lore0 = new ArrayList<String>();
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&fCurrency: &a$250"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&fVote Credits: &a2 Vote Credit"));
		ItemStack item0 = createItem(Material.PAPER, "&eCurrency Exchange", lore0);
		
		List<String> lore1 = new ArrayList<String>();
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&fCurrency: &a$400"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&fVote Credits: &a3 Vote Credit"));
		ItemStack item1 = createItem(Material.PAPER, "&eCurrency Exchange", lore1);
		
		List<String> lore2 = new ArrayList<String>();
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&fCurrency: &a$800"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&fVote Credits: &a5 Vote Credit"));
		ItemStack item2 = createItem(Material.PAPER, "&eCurrency Exchange", lore2);
		
		List<String> lore3 = new ArrayList<String>();
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&fCurrency: &a$2,000"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&fVote Credits: &a10 Vote Credit"));
		ItemStack item3 = createItem(Material.PAPER, "&eCurrency Exchange", lore3);
		
		List<String> lore4 = new ArrayList<String>();
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&fCurrency: &a$5,000"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&8-+-"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&fVote Credits: &a20 Vote Credit"));
		ItemStack item4 = createItem(Material.PAPER, "&eCurrency Exchange", lore4);
		ItemStack gui = createItem(Material.BARRIER, ChatColor.RED + "Back to Credit Master");
		
		SurvivalPlayer sPlayer = SurvivalPlayerManager.getPlayer(player.getUniqueId());
		ClickableItem item0Click = ClickableItem.of(item0, e -> {
			hasVoteCredits(sPlayer, 2, 250);
		});
		ClickableItem item1Click = ClickableItem.of(item1, e -> {
			hasVoteCredits(sPlayer, 3, 400);
		});
		ClickableItem item2Click = ClickableItem.of(item2, e -> {
			hasVoteCredits(sPlayer, 5, 800);
		});
		ClickableItem item3Click = ClickableItem.of(item3, e -> {
			hasVoteCredits(sPlayer, 10, 2000);
		});
		ClickableItem item4Click = ClickableItem.of(item4, e -> {
			hasVoteCredits(sPlayer, 20, 5000);
		});
		
		cont.set(1, 2, item0Click);
		cont.set(1, 3, item1Click);
		cont.set(1, 4, item2Click);
		cont.set(1, 5, item3Click);
		cont.set(1, 6, item4Click);
		cont.set(2, 4, ClickableItem.of(gui, e -> CreditMasterInventory.INVENTORY.open(player)));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}

	private void hasVoteCredits(SurvivalPlayer player, int voteCredits, int currency) {
		int playerCredits = player.getGamePlayer().getVoteCredits();
		if(playerCredits >= voteCredits) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + player.getPlayer().getName() + " " + currency);
			player.getGamePlayer().setVoteCredits(playerCredits - voteCredits);
			player.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&aKC&8] You've exchanged &a" + voteCredits + " Vote Credit(s) &8for &a$" + currency));
		} else {
			player.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&aKC&8] You need more &aVote Credits &8for that!"));
		}
	}
	
	private ItemStack createItem(Material m, String name, List<String> lores) {
		ItemStack item = new ItemStack(m);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		meta.setLore(lores);
		item.setItemMeta(meta);
		return item;
	}
	
	private ItemStack createItem(Material m, String name) {
		ItemStack item = new ItemStack(m);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		item.setItemMeta(meta);
		return item;
	}
}