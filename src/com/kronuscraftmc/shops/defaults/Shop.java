package com.kronuscraftmc.shops.defaults;

import org.bukkit.Material;

public enum Shop {
	// Use this format for new shops, the rest of the code should handle everything else
	// SHOP_NAME ("Shop Title", String[] {"lore", "items"}, UI Material, int[] {row, column}, INVENTORY)

	BLOCKS (
		"Block", 
		new String [] {"From Obsidian to Wool","A vast variety of Minecraft blocks!"},
		Material.STONE,
		new int [] {1,1}
		),
	ORE (
		"Ore",
		new String[] {"Diamonds, diamonds, diamonds!"},
		Material.EMERALD,
		new int [] {1,2}
		),
	MOBS (
		"Mob",
		new String[] {"Diamonds, diamonds, diamonds!"},
		Material.BONE,
		new int[] {1,3}
		),

	FARMING (
		"Farm",
		new String [] {"Get your farm going.","Seeds, food, all you need!"},
		Material.DIAMOND_HOE,
		new int[] {1,4}
	),
	REDSTONE (
		"Redstone",
		new String [] {"Everything that connects to","power found here!"},
		Material.REDSTONE_BLOCK,
		new int[] {1,5}
	),
	NETHER(
		"Nether",
		new String [] {"Save the journey to the","underworld! Much easier."},
		Material.NETHERRACK,
		new int[] {1,6}
	),
	SPAWNER(
		"Spawner",
		new String [] {"Spawners available here!","Zombie, Enderman, Skeleton and more!"},
		Material.SPAWNER,
		new int[] {1,7}
	),
	MISC(
		"Misc",
		new String [] {"Anything extra to be sold!"},
		Material.QUARTZ,
		new int[] {2,3}
	),
	ENCHANTING(
		"Enchanting",
		new String [] {"Enchanting tables, XP bottles","and Enchanted books on sale!"},
		Material.ENCHANTED_BOOK,
		new int[] {2,5}
	);

	public final String name;
	public final String title;
	public final String[] loreLines;
	public final Material guiObject;
	public final int[] containerCoordinates;
	Shop(String name, String[] loreLines, Material guiObject, int[] containerCoordinates){
		this.name = name;
		this.title = name + " Shop";
		this.loreLines = loreLines;
		this.guiObject = guiObject;
		this.containerCoordinates = containerCoordinates;
	}

}
