package com.kronuscraftmc;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.kronuscraftmc.kronuscraft.KronusCraft;
import com.kronuscraftmc.kronuscraft.database.SurvivalPlayerData;
import com.kronuscraftmc.kronuscraft.players.GamePlayer;
import com.kronuscraftmc.ranks.RankList;

import me.confuser.barapi.BarAPI;
import me.glaremasters.guilds.api.GuildsAPI;

public class SurvivalPlayer {

	private Player player;
	private GamePlayer gPlayer;
	private Scoreboard board;
	public int scoreboardMoney, scoreboardRank, scoreboardCredits, scoreboardVotes;
	private String guildName;
	private RankList rank;

	public SurvivalPlayer(UUID uuid) {
		this.player = Bukkit.getPlayer(uuid);
		this.gPlayer = KronusCraft.getInstance().getPlayerManager().getGamePlayer(uuid);
		setupRank();
		this.scoreboardMoney = (int) getMoney();
		this.scoreboardRank = getGamePlayer().getRank().getId();
		this.scoreboardCredits = getGamePlayer().getVoteCredits();
		this.scoreboardVotes = Survival.getInstance().getVoteParty().getRemainingVotes();

		setupGuildName();
		setupScoreboard();
		setupBossBars();
	}

	public void setupBossBars() {
		BarAPI.removeBar(player);

		//BarAPI.setMessage(player, ChatColor.translateAlternateColorCodes('&', "&e25% Sale in the Ranks Store!"));
		
		if(rank == RankList.GODLY) {
			BarAPI.setMessage(player, ChatColor.translateAlternateColorCodes('&', "&eMAX RANK REACHED"));
			return;
		}
		RankList nextRank = RankList.getPlayerRank(rank.getId() + 1);
		double needed = nextRank.getCost() - Survival.getInstance().getEconomy().getBalance(player);
		if(needed > 0) {
		BarAPI.setMessage(player, ChatColor.translateAlternateColorCodes('&', "&fNext Rank:&e " +
		nextRank.getTag(false, false, nextRank.getChatColor()) + "&7 (&a$" + getMoneyTag(needed) + "&7 for next Rank)"), 0F);
		} else {
			BarAPI.setMessage(player, ChatColor.translateAlternateColorCodes('&', "&eNEXT RANK AVALIALBE"));
		}
		
	}

	private void setupGuildName() {
		GuildsAPI api = Survival.getInstance().getGuildsAPI();
		if(api.getGuild(player).startsWith(" ")) {
			this.guildName = "None";
			return;
		}
		this.guildName = api.getGuild(player);	
	}

	public void setupScoreboard() {
		if (board != null) {
			if (player.getPlayer().getScoreboard().getObjective(DisplaySlot.SIDEBAR) == board.getObjective(DisplaySlot.SIDEBAR)) {
				player.getPlayer().getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
			}
		}
		
		board = Bukkit.getScoreboardManager().getNewScoreboard();
		@SuppressWarnings("deprecation")
		Objective obj = board.registerNewObjective("new", "new");
		obj.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&b&lSurvival"));
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		obj.getScore(ChatColor.RED + " ").setScore(13);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&eRank: " + getRank().getTag(false, false, getRank().getChatColor()))).setScore(12);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&eVote Credits: &r&b" + scoreboardCredits)).setScore(11);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&eMoney: &r&f$" + getMoneyTag(scoreboardMoney))).setScore(10);
		obj.getScore(ChatColor.GREEN + " ").setScore(9);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&eVote Party")).setScore(8);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&f" + Survival.getInstance().getVoteParty().getRemainingVotes() + " Votes Remaining")).setScore(7);
		obj.getScore(ChatColor.GRAY + " ").setScore(6);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&eGuild Name")).setScore(5);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&f" + guildName)).setScore(4);
		obj.getScore(ChatColor.BLUE + " ").setScore(3);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "&eWebsite")).setScore(2);
		obj.getScore(ChatColor.translateAlternateColorCodes('&', "kronuscraftmc.com")).setScore(1);
		player.getPlayer().setScoreboard(board);
	}

	public String getMoneyTag(double number) {
		DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
		symbols.setGroupingSeparator(',');
		formatter.setDecimalFormatSymbols(symbols);	
		return formatter.format(number);
	}
	
	public Player getPlayer() {
		return player;
	}

	public GamePlayer getGamePlayer() {
		return gPlayer;
	}
	
	public boolean checkCredits(int credits) {
		return scoreboardCredits == credits;
	}
	
	public int getScoreboardCredits() {
		return scoreboardCredits;
	}

	public void setScoreboardCredits(int scoreboardCredits) {
		this.scoreboardCredits = scoreboardCredits;
	}
	
	public boolean checkRank(int id) {
		return this.scoreboardRank == id;
	}
	
	public int getScoreboardRank() {
		return scoreboardRank;
	}

	public void setScoreboardRank(int rankid) {
		this.scoreboardRank = rankid;
	}

	public boolean checkMoney(int money) {
		return scoreboardMoney == money;
	}
	
	public int getScoreboardMoney() {
		return scoreboardMoney;
	}

	public void setScoreboardMoney(int money) {
		this.scoreboardMoney = money;
	}
	
	public boolean checkVotes(int votes) {
		return scoreboardVotes == votes;
	}
	
	
	public int getScoreboardVotes() {
		return scoreboardVotes;
	}
	
	public void setScoreboardVotes(int scoreboardVotes) {
		this.scoreboardVotes = scoreboardVotes;
	}
	
	public String getGuildName() {
		return guildName;
	}
	
	public void setGuildName(String guildName) {
		this.guildName = guildName;
	}
	
	public boolean checkGuildName(String name) {
		if(name.equals(guildName)) {
			return true;
		}
		return false;
	}
	
	private void setupRank() {
		int rankId = SurvivalPlayerData.getSurvivalGamePlayerRank(player.getUniqueId());
		System.out.println("DB Survival rank: " + rankId);
		this.rank = RankList.getPlayerRank(rankId);
	}
	
	public RankList getRank() {
		return rank;
	}
	
	public void setRank(RankList rank) {
		SurvivalPlayerData.updateSurvivalRank(player.getUniqueId(), rank.getId());
		this.rank = rank;
	}

	public void addMoney(double money) {
		Survival.getInstance().getEconomy().depositPlayer(player, money);
	}

	public void removeMoney(double money) {
		Survival.getInstance().getEconomy().withdrawPlayer(player, money);
	}

	public double getMoney() {
		return Survival.getInstance().getEconomy().getBalance(player);
	}
}
