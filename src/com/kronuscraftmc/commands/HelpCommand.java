package com.kronuscraftmc.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.kronuscraftmc.inventorys.HelpInventory;

public class HelpCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		Player player = (Player) s;
		
		HelpInventory.INVENTORY.open(player);

		return true;
	}
}
