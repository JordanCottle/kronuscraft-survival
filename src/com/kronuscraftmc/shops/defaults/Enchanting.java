package com.kronuscraftmc.shops.defaults;

import org.bukkit.configuration.file.FileConfiguration;

public enum Enchanting {
	PROTECTION4("Protection", 400, 4),
	PROTECTION3("Protection", 300, 3),
	FIREPROTECTION1("Fire Protection", 250, 1),
	FIREPROTECTION2("Fire Protection", 400, 2),
	FIREPROTECTION3("Projectile Protection", 400, 3),
	PROJECTILEPROTECTION2("Projectile Protection", 300, 2),
	THORNS2("Thorns", 500, 2),
	THORNS1("Thorns", 300, 1),
	RESPIRATION("Respiration", 250, 2),
	SHARP2("Sharpness", 250, 2),
	SHARP3("Sharpness", 350, 3),
	SHARP4("Sharpness", 500, 4),
	KNOCKBACK1("Knockback", 250, 1),
	KNOCKBACK2("Knockback", 400, 2),
	LOOT1("Looting", 350, 1),
	LOOT2("Looting", 500, 2),
	FIREASPECT1("Fire Aspect", 500, 1),
	FIREASPECT2("Fire Aspect", 750, 2),
	SILKTOUCH("Silk Touch", 750, 1),
	EFFICIENCY2("Efficiency", 350, 2),
	EFFICIENCY3("Efficiency", 450, 3),
	EFFICIENCY4("Efficiency", 600, 4),
	FORTUNE1("Fortune", 500, 1),
	FORTUNE2("Fortune", 750, 2),
	FORTUNE3("Fortune", 1000, 3),
	UNBREAKING2("Unbreaking", 500, 2),
	MENDING("Mending", 1000, 1),
	INFINITY("Infinity", 1000, 1),
	POWER2("Power", 350, 2),
	POWER3("Power", 500, 3),
	POWER4("Power", 750, 4),
	PUNCH1("Punch", 500, 1),
	PUNCH2("Punch", 750, 2),
	FLAME("Flame", 1000, 1);
	
	public final String name;
	public final double buyPrice;
	public final double sellPrice;
	public final int level;
	Enchanting(String name, double buyPrice, int level){
		this.name = name;
		this.buyPrice = buyPrice;
		this.sellPrice = 0;
		this.level = level;
	}
	
	public static void setUp(FileConfiguration fileConfig) {
		for(Enchanting item: Enchanting.values()) {
			fileConfig.addDefault(item.name + ".BuyPrice", item.buyPrice);
			fileConfig.addDefault(item.name + ".SellPrice", item.sellPrice);
			fileConfig.addDefault(item.name + ".Level", item.level);
		}
	}
}
