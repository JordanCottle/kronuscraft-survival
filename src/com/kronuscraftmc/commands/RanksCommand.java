package com.kronuscraftmc.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.kronuscraftmc.SurvivalPlayer;
import com.kronuscraftmc.SurvivalPlayerManager;

public class RanksCommand  implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command arg1, String arg2, String[] arg3) {
		SurvivalPlayer player = SurvivalPlayerManager.getPlayer(s.getName());
		s.sendMessage(ChatColor.GRAY + "Your current rank is: " + player.getRank().getTag(true, false, player.getRank().getChatColor()));
		s.sendMessage(ChatColor.GRAY + "Use command " + ChatColor.AQUA + "/rankup | /ranks" + ChatColor.GRAY + " to rankup!");
		return false;
	}
}
