package com.kronuscraftmc.shops;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.shops.defaults.Block;
import com.kronuscraftmc.shops.defaults.Enchanting;
import com.kronuscraftmc.shops.defaults.Farm;
import com.kronuscraftmc.shops.defaults.Misc;
import com.kronuscraftmc.shops.defaults.Mob;
import com.kronuscraftmc.shops.defaults.Nether;
import com.kronuscraftmc.shops.defaults.Ore;
import com.kronuscraftmc.shops.defaults.Redstone;
import com.kronuscraftmc.shops.defaults.SpawnEgg;
import com.kronuscraftmc.shops.defaults.Spawner;

public class ShopConfig {
	private static String LOG_HEADER = "[ShopConfig] ";
	
	private File file;
	private FileConfiguration fileConfig;
	private String name;
	
	private BuyableItem [] items;

	public ShopConfig(String shopName) {
		String path = Survival.getInstance().getDataFolder() + "/shops/"+ shopName + ".yml";
		this.file = new File(path);
		
		
		// check if Shops config directory exists and create it if missing
		if(!file.getParentFile().exists()){
			log("Shops directory not found; initializing new Shops directory");
			file.getParentFile().mkdirs();
		}
		
		// check for specific shop's config file, and create it if not found
		if (!file.exists()) { // set up new config file with default values
			log(shopName + ".yml config file not found; creating new configuration file");
			
			try {
				file.createNewFile();
			} catch (IOException e) {
				log("File creation failed");
				Bukkit.getPluginManager().disablePlugin(Survival.getInstance());
				return;
			}
			this.fileConfig = YamlConfiguration.loadConfiguration(file);
			
			switch(shopName) {
				case "Block":
					Block.setUp(fileConfig);
					break;
				case "Ore":
					Ore.setUp(fileConfig);
					break;
				case "Redstone":
					Redstone.setUp(fileConfig);
					break;
				case "Mob":
					Mob.setUp(fileConfig);
					break;
				case "Farm":
					Farm.setUp(fileConfig);
					break;
				case "Nether":
					Nether.setUp(fileConfig);
					break;
				case "Misc":
					Misc.setUp(fileConfig);
					break;
				case "Enchanting":
					Enchanting.setUp(fileConfig);
					break;
				case "Spawner":
					Spawner.setUp(fileConfig);
					SpawnEgg.setUp(fileConfig);
					break;
				default:
					log("No default setup for " + shopName + " found, empty config file created");
			}
			
			fileConfig.options().copyDefaults(true);
			
			try {
				fileConfig.save(file);
			}
			catch(IOException e) {
				log("File saving failed!");
				Bukkit.getPluginManager().disablePlugin(Survival.getInstance());
				return;
			}
			
		}
		else {
			log(shopName + " configs found!");
			this.fileConfig = YamlConfiguration.loadConfiguration(file);
		}
		
		// generate BuyableItems from the config file information
		Set<String> itemNames = fileConfig.getKeys(false);
		
		BuyableItem [] items = new BuyableItem[itemNames.size()];
		int itemIndex = 0;
		for (String itemName: itemNames) {
			double buyPrice = fileConfig.getDouble(itemName + ".BuyPrice");
			double sellPrice = fileConfig.getDouble(itemName + ".SellPrice");
			
			items[itemIndex++] = new BuyableItem(itemName, buyPrice, sellPrice);
		}
		this.items = items;
	}
	
	private static void log(String message) {
		System.out.println(LOG_HEADER + message);
	}

	public String getName() {
		return this.name;
	}

	public File getFile() {
		return file;
	}

	public FileConfiguration getFileConfig() {
		return fileConfig;
	}
	
	public BuyableItem[] getItems() {
		return items;
	}
}
