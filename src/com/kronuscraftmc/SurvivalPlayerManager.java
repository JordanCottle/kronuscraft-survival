package com.kronuscraftmc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SurvivalPlayerManager {
	public static List<SurvivalPlayer> players = new ArrayList<SurvivalPlayer>();
	
	public static List<SurvivalPlayer> getPlayers() {
		return players;
	}
	
	public static void addPlayer(SurvivalPlayer player) {
		if(!players.contains(player)) {
			players.add(player);
		}
	}
	
	public static boolean removePlayer(SurvivalPlayer player) {
		return players.remove(player);
	}
	
	public static SurvivalPlayer getPlayer(UUID uuid) {
		for (SurvivalPlayer p : players) {
			if (p.getPlayer().getUniqueId().equals(uuid)) {
				return p;
			}
		}
		return null;
	}
	
	public static SurvivalPlayer getPlayer(String name) {
		for (SurvivalPlayer p : players) {
			if (p.getPlayer().getName().equals(name)) {
				return p;
			}
		}
		return null;
	}
}
