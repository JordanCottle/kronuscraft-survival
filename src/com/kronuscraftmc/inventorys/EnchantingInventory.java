package com.kronuscraftmc.inventorys;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import com.kronuscraftmc.shops.ShopNav;
import com.kronuscraftmc.shops.BaseInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class EnchantingInventory extends BaseInventory {

	public EnchantingInventory(){
		super.icon = createItem(Material.ENCHANTED_BOOK, SHOP_NAME_COLOR + "Enchanting Shop", createLore(SHOP_DESCRIPTION_COLOR, NAV_HEADING, new String[] {"Enchanting tables, XP bottles","and Enchanted books on sale!"}));
		super.navCoordinates = new int[] {2,5};
		super.inventory = SmartInventory.builder().id("enchant").provider(this)
				.size(4, 9).title(ChatColor.AQUA + "Enchanting Shop").build();
	}

	@Override
	public void init(Player player, InventoryContents cont) {
		List<String> lore1 = new ArrayList<String>();
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$400"));
		ItemStack item1 = createItem(Material.ENCHANTED_BOOK, "&fProtection IV", lore1);

		List<String> lore2 = new ArrayList<String>();
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$300"));
		ItemStack item2 = createItem(Material.ENCHANTED_BOOK, "&fProtection III", lore2);
		
		List<String> lore3 = new ArrayList<String>();
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$250"));
		ItemStack item3 = createItem(Material.ENCHANTED_BOOK, "&fFire Protection I", lore3);
		
		List<String> lore4 = new ArrayList<String>();
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$400"));
		ItemStack item4 = createItem(Material.ENCHANTED_BOOK, "&fFire Protection II", lore4);
		
		List<String> lore5 = new ArrayList<String>();
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$400"));
		ItemStack item5 = createItem(Material.ENCHANTED_BOOK, "&fProjectile Protection III", lore5);
		
		List<String> lore6 = new ArrayList<String>();
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$300"));
		ItemStack item6 = createItem(Material.ENCHANTED_BOOK, "&fProjectile Protection II", lore6);
		
		List<String> lore7 = new ArrayList<String>();
		lore7.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$500"));
		ItemStack item7 = createItem(Material.ENCHANTED_BOOK, "&fThorns II", lore7);
		
		List<String> lore8 = new ArrayList<String>();
		lore8.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$300"));
		ItemStack item8 = createItem(Material.ENCHANTED_BOOK, "&fThrons I", lore8);

		List<String> lore9 = new ArrayList<String>();
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$250"));
		ItemStack item9 = createItem(Material.ENCHANTED_BOOK, "&fRespiration II", lore9);
		
		List<String> lore10 = new ArrayList<String>();
		lore10.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$250"));
		ItemStack item10 = createItem(Material.ENCHANTED_BOOK, "&fSharpness II", lore10);
		
		List<String> lore11 = new ArrayList<String>();
		lore11.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$350"));
		ItemStack item11 = createItem(Material.ENCHANTED_BOOK, "&fSharpness III", lore11);
		
		List<String> lore12 = new ArrayList<String>();
		lore12.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$500"));
		ItemStack item12 = createItem(Material.ENCHANTED_BOOK, "&fSharpness IV", lore12);
		
		List<String> lore13 = new ArrayList<String>();
		lore13.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$250"));
		ItemStack item13 = createItem(Material.ENCHANTED_BOOK, "&fKnockback I", lore13);
		
		List<String> lore14 = new ArrayList<String>();
		lore14.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$400"));
		ItemStack item14 = createItem(Material.ENCHANTED_BOOK, "&fKnockack II", lore14);
		
		List<String> lore15 = new ArrayList<String>();
		lore15.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$350"));
		ItemStack item15 = createItem(Material.ENCHANTED_BOOK, "&fLooting I", lore15);
		
		List<String> lore16 = new ArrayList<String>();
		lore16.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$500"));
		ItemStack item16 = createItem(Material.ENCHANTED_BOOK, "&fLooting II", lore16);
		
		List<String> lore17 = new ArrayList<String>();
		lore17.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$500"));
		ItemStack item17 = createItem(Material.ENCHANTED_BOOK, "&fFire Aspect I", lore17);
		
		List<String> lore18 = new ArrayList<String>();
		lore18.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$750"));
		ItemStack item18 = createItem(Material.ENCHANTED_BOOK, "&fFire Aspect II", lore18);
		
		List<String> lore19 = new ArrayList<String>();
		lore19.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$750"));
		ItemStack item19 = createItem(Material.ENCHANTED_BOOK, "&fSilk Touch I", lore19);

		List<String> lore20 = new ArrayList<String>();
		lore20.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$350"));
		ItemStack item20 = createItem(Material.ENCHANTED_BOOK, "&fEfficiency II", lore20);
		
		List<String> lore21 = new ArrayList<String>();
		lore21.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$450"));
		ItemStack item21 = createItem(Material.ENCHANTED_BOOK, "&fEfficiency III", lore21);
		
		List<String> lore22 = new ArrayList<String>();
		lore22.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$600"));
		ItemStack item22 = createItem(Material.ENCHANTED_BOOK, "&fEfficiency IV", lore22);

		List<String> lore23 = new ArrayList<String>();
		lore23.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$500"));
		ItemStack item23 = createItem(Material.ENCHANTED_BOOK, "&fFortune I", lore23);
		
		List<String> lore24 = new ArrayList<String>();
		lore24.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$750"));
		ItemStack item24 = createItem(Material.ENCHANTED_BOOK, "&fFortune II", lore24);

		List<String> lore25 = new ArrayList<String>();
		lore25.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$1,000"));
		ItemStack item25 = createItem(Material.ENCHANTED_BOOK, "&fFortune III", lore25);
		
		List<String> lore26 = new ArrayList<String>();
		lore26.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$500"));
		ItemStack item26 = createItem(Material.ENCHANTED_BOOK, "&fUnbreaking II", lore26);
		
		List<String> lore27 = new ArrayList<String>();
		lore27.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$1,000"));
		ItemStack item27 = createItem(Material.ENCHANTED_BOOK, "&fMending I", lore27);
		
		List<String> lore28 = new ArrayList<String>();
		lore28.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$1,000"));
		ItemStack item28 = createItem(Material.ENCHANTED_BOOK, "&fInfinity I", lore28);

		List<String> lore30 = new ArrayList<String>();
		lore30.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$350"));
		ItemStack item30 = createItem(Material.ENCHANTED_BOOK, "&fPower II", lore30);
		
		List<String> lore31 = new ArrayList<String>();
		lore31.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$500"));
		ItemStack item31 = createItem(Material.ENCHANTED_BOOK, "&fPower III", lore31);
		
		List<String> lore32 = new ArrayList<String>();
		lore32.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$750"));
		ItemStack item32 = createItem(Material.ENCHANTED_BOOK, "&fPower IV", lore32);
		
		List<String> lore33 = new ArrayList<String>();
		lore33.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$500"));
		ItemStack item33 = createItem(Material.ENCHANTED_BOOK, "&fPunch I", lore33);
		
		List<String> lore34 = new ArrayList<String>();
		lore34.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$750"));
		ItemStack item34 = createItem(Material.ENCHANTED_BOOK, "&fPunch II", lore34);
		
		List<String> lore35 = new ArrayList<String>();
		lore35.add(ChatColor.translateAlternateColorCodes('&', "&eBuy Price: &a&o$1,000"));
		ItemStack item35 = createItem(Material.ENCHANTED_BOOK, "&fFlame I", lore35);
		
		ItemStack gui = createItem(Material.BARRIER, ChatColor.RED + "Back to Shop");

		ClickableItem item1Click = ClickableItem.of(item1, e -> {
			sellItem(player, 400.0, Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		});
		ClickableItem item2Click = ClickableItem.of(item2, e -> {
			sellItem(player, 300.0, Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		});
		ClickableItem item3Click = ClickableItem.of(item3, e -> {
			sellItem(player, 250.0, Enchantment.PROTECTION_FIRE, 1);
		});
		ClickableItem item4Click = ClickableItem.of(item4, e -> {
			sellItem(player, 400.0, Enchantment.PROTECTION_FIRE, 2);
		});
		ClickableItem item5Click = ClickableItem.of(item5, e -> {
			sellItem(player, 400.0, Enchantment.PROTECTION_PROJECTILE, 3);
		});
		ClickableItem item6Click = ClickableItem.of(item6, e -> {
			sellItem(player, 300.0, Enchantment.PROTECTION_PROJECTILE, 2);
		});
		ClickableItem item7Click = ClickableItem.of(item7, e -> {
			sellItem(player, 500.0, Enchantment.THORNS, 2);
		});
		ClickableItem item8Click = ClickableItem.of(item8, e -> {
			sellItem(player, 300.0, Enchantment.THORNS, 1);
		});
		ClickableItem item9Click = ClickableItem.of(item9, e -> {
			sellItem(player, 250.0, Enchantment.OXYGEN, 2);
		});
		ClickableItem item10Click = ClickableItem.of(item10, e -> {
			sellItem(player, 250.0, Enchantment.DAMAGE_ALL, 2);
		});
		ClickableItem item11Click = ClickableItem.of(item11, e -> {
			sellItem(player, 350.0, Enchantment.DAMAGE_ALL, 3);
		});
		ClickableItem item12Click = ClickableItem.of(item12, e -> {
			sellItem(player, 500.0, Enchantment.DAMAGE_ALL, 4);
		});
		ClickableItem item13Click = ClickableItem.of(item13, e -> {
			sellItem(player, 250.0, Enchantment.KNOCKBACK, 1);
		});
		ClickableItem item14Click = ClickableItem.of(item14, e -> {
			sellItem(player, 400.0, Enchantment.KNOCKBACK, 2);
		});
		ClickableItem item15Click = ClickableItem.of(item15, e -> {
			sellItem(player, 350.0, Enchantment.LOOT_BONUS_MOBS, 1);
		});
		ClickableItem item16Click = ClickableItem.of(item16, e -> {
			sellItem(player, 500.0, Enchantment.LOOT_BONUS_MOBS, 2);
		});
		ClickableItem item17Click = ClickableItem.of(item17, e -> {
			sellItem(player, 500.0, Enchantment.FIRE_ASPECT, 1);
		});
		ClickableItem item18Click = ClickableItem.of(item18, e -> {
			sellItem(player, 750.0, Enchantment.FIRE_ASPECT, 2);
		});
		ClickableItem item19Click = ClickableItem.of(item19, e -> {
			sellItem(player, 750.0, Enchantment.SILK_TOUCH, 1);
		});
		ClickableItem item20Click = ClickableItem.of(item20, e -> {
			sellItem(player, 350.0, Enchantment.DIG_SPEED, 2);
		});
		ClickableItem item21Click = ClickableItem.of(item21, e -> {
			sellItem(player, 350.0, Enchantment.DIG_SPEED, 3);
		});
		ClickableItem item22Click = ClickableItem.of(item22, e -> {
			sellItem(player, 350.0, Enchantment.DIG_SPEED, 4);
		});
		ClickableItem item23Click = ClickableItem.of(item23, e -> {
			sellItem(player, 500.0, Enchantment.LOOT_BONUS_BLOCKS, 1);
		});
		ClickableItem item24Click = ClickableItem.of(item24, e -> {
			sellItem(player, 750.0, Enchantment.LOOT_BONUS_BLOCKS, 2);
		});
		ClickableItem item25Click = ClickableItem.of(item25, e -> {
			sellItem(player, 1000.0, Enchantment.LOOT_BONUS_BLOCKS, 3);
		});
		ClickableItem item26Click = ClickableItem.of(item26, e -> {
			sellItem(player, 500.0, Enchantment.DURABILITY, 2);
		});
		ClickableItem item27Click = ClickableItem.of(item27, e -> {
			sellItem(player, 1000.0, Enchantment.MENDING, 1);
		});
		ClickableItem item28Click = ClickableItem.of(item28, e -> {
			sellItem(player, 1000.0, Enchantment.ARROW_INFINITE, 1);
		});
		ClickableItem item30Click = ClickableItem.of(item30, e -> {
			sellItem(player, 350.0, Enchantment.ARROW_DAMAGE, 2);
		});
		ClickableItem item31Click = ClickableItem.of(item31, e -> {
			sellItem(player, 500.0, Enchantment.ARROW_DAMAGE, 3);
		});
		ClickableItem item32Click = ClickableItem.of(item32, e -> {
			sellItem(player, 750.0, Enchantment.ARROW_DAMAGE, 4);
		});
		ClickableItem item33Click = ClickableItem.of(item33, e -> {
			sellItem(player, 500.0, Enchantment.ARROW_KNOCKBACK, 1);
		});
		ClickableItem item34Click = ClickableItem.of(item34, e -> {
			sellItem(player, 750.0, Enchantment.ARROW_KNOCKBACK, 2);
		});
		ClickableItem item35Click = ClickableItem.of(item35, e -> {
			sellItem(player, 1000.0, Enchantment.ARROW_FIRE, 1);
		});
		
		cont.set(0, 0, item1Click);
		cont.set(0, 1, item2Click);
		cont.set(0, 2, item3Click);
		cont.set(0, 3, item4Click);
		cont.set(0, 4, item5Click);
		cont.set(0, 5, item6Click);
		cont.set(0, 6, item7Click);
		cont.set(0, 7, item8Click);
		cont.set(0, 8, item9Click);
		cont.set(1, 0, item10Click);
		cont.set(1, 1, item11Click);
		cont.set(1, 2, item12Click);
		cont.set(1, 3, item13Click);
		cont.set(1, 4, item14Click);
		cont.set(1, 5, item15Click);
		cont.set(1, 6, item16Click);
		cont.set(1, 7, item17Click);
		cont.set(1, 8, item18Click);
		cont.set(2, 0, item19Click);
		cont.set(2, 1, item20Click);
		cont.set(2, 2, item21Click);
		cont.set(2, 3, item22Click);
		cont.set(2, 4, item23Click);
		cont.set(2, 5, item24Click);
		cont.set(2, 6, item25Click);
		cont.set(2, 7, item26Click);
		cont.set(2, 8, item27Click);
		cont.set(3, 0, item28Click);
		cont.set(3, 1, item30Click);
		cont.set(3, 2, item31Click);
		cont.set(3, 3, item32Click);
		cont.set(3, 4, item33Click);
		cont.set(3, 5, item34Click);
		cont.set(3, 6, item35Click);
		cont.set(3, 8, ClickableItem.of(gui, e -> ShopNav.INVENTORY.open(player)));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {

	}
	
	
}
