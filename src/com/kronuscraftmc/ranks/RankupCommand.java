package com.kronuscraftmc.ranks;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.kronuscraftmc.inventorys.RankupInventory;

public class RankupCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		Player player = (Player) s;
		RankupInventory.INVENTORY.open(player);
		return true;
	}
}
