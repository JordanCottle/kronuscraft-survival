package com.kronuscraftmc.shops.defaults;

import org.bukkit.configuration.file.FileConfiguration;

public enum Ore {
	COAL("COAL", 7.50, 2.0),
	IRON_INGOT("IRON_INGOT", 15.0, 3.0),
	GOLD_INGOT("GOLD_INGOT", 25.0, 5.0),
	REDSTONE("REDSTONE", 10.0, 2.0),
	EMERALD("EMERALD", 100.0, 15.0),
	DIAMOND("DIAMOND", 125.0, 20.0),
	QUARTZ("QUARTZ", 10.0, 2.0),
	LAPIS_LAZULI("LAPIS_LAZULI", 20.0, 5.0),
	GLOWSTONE_DUST("GLOWSTONE_DUST", 5.0, 2.50);
	
	public final String name;
	public final double buyPrice;
	public final double sellPrice;
	Ore(String name, double buyPrice, double sellPrice){
		this.name = name;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
	}
	
	public static void setUp(FileConfiguration fileConfig) {
		for(Ore item: Ore.values()) {
			fileConfig.addDefault(item.name + ".BuyPrice", item.buyPrice);
			fileConfig.addDefault(item.name + ".SellPrice", item.sellPrice);
		}
	}
}
