package com.kronuscraftmc.inventorys;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.inventorys.credits.CreditMasterInventory;
import com.kronuscraftmc.inventorys.donate.DonateInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;

public class HelpInventory implements InventoryProvider {

	public static final SmartInventory INVENTORY = SmartInventory.builder().id("help").provider(new HelpInventory())
			.size(6, 9).title(ChatColor.RED + "Help GUI").build();

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		
		List<String> machine = new ArrayList<String>();
		machine.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		machine.add(ChatColor.translateAlternateColorCodes('&', "&fView a list of custom machinery"));
		machine.add(ChatColor.translateAlternateColorCodes('&', "&fsuch as Quarrys, Recycler"));
		machine.add(ChatColor.translateAlternateColorCodes('&', "&fAuto Crafting Table, Macerator,"));
		machine.add(ChatColor.translateAlternateColorCodes('&', "&fCustom Chests, Pipes & much more!"));
		ItemStack itemMachine = createItem(Material.PISTON, ChatColor.translateAlternateColorCodes('&', "&aCustom Machines"), machine);
		
		List<String> lore0 = new ArrayList<String>();
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&f + /help &7&o(Opens the help menu)"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&f + /machines &7&o(Opens a GUI for Machines)"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&f + /rankup | /rank &7&o(Opens the Rank GUI)"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&f + /vote &7&o(Vote for Keys, Credits & Money)"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&f + /claim &7&o(Claims land 5x5 around you)"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&f + /credits &7&o(Open the Credits GUI)"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&f + /wild &7&o(Teleports you to a random area)"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&f + /shop &7&o(Opens the admin shop)"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&f + /kit &7&o(Opens the Kits GUI)"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&f + /jobs browse &7&o(Opens jobs menu)"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&f + /warp mining &7&o(Public mining area)"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&f + /warp enchant &7&o(Public enchanting area)"));
		lore0.add(ChatColor.translateAlternateColorCodes('&', "&f + /spawn &7&o(Teleports you to spawn)"));
		ItemStack item0 = createItem(Material.PAPER, ChatColor.translateAlternateColorCodes('&', "&aCommands"), lore0);

		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore.add(ChatColor.translateAlternateColorCodes('&', "&fTeleport to a random"));
		lore.add(ChatColor.translateAlternateColorCodes('&', "&flocation in the world"));
		ItemStack item1 = createItem(Material.COMPASS, ChatColor.translateAlternateColorCodes('&', "&aWilderness"), lore);
		
		List<String> lore1 = new ArrayList<String>();
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&fSupport the development!"));
		lore1.add(ChatColor.translateAlternateColorCodes('&', "&fRanks, Credits and more!"));
		ItemStack item3 = createItem(Material.ENDER_CHEST, ChatColor.translateAlternateColorCodes('&', "&aDonator Shop"), lore1);
		
		
		List<String> lore2 = new ArrayList<String>();
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&f&oJoin a Guild or create"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&f&oyour own! Guilds is a custom"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&f&ofeature that allows our"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&f&oplayers to ally with one another!"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&f&oUse the Guild to upgrade Tiers,"));
		lore2.add(ChatColor.translateAlternateColorCodes('&', "&f&orescources and buy perks!"));
		ItemStack item2 = createItem(Material.WRITABLE_BOOK, ChatColor.translateAlternateColorCodes('&', "&aGuilds"), lore2);
		
		List<String> lore3 = new ArrayList<String>();
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&fMore homes! New in-game tag."));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&fClick to find out how"));
		lore3.add(ChatColor.translateAlternateColorCodes('&', "&fto become a Member!"));
		ItemStack item5 = createItem(Material.CHAINMAIL_HELMET, ChatColor.translateAlternateColorCodes('&', "&aMember Rank"), lore3);

		List<String> lore4 = new ArrayList<String>();
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&fBuy or sell your"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&funwanted items. Purchase"));
		lore4.add(ChatColor.translateAlternateColorCodes('&', "&fa variety of items here!"));
		ItemStack item6 = createItem(Material.CHEST, ChatColor.translateAlternateColorCodes('&', "&aShop"), lore4);
		
		List<String> lore5 = new ArrayList<String>();
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&fFree open-to-all"));
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&fpublic mine. Ores"));
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&fsuch as Coal, Iron, Gold"));
		lore5.add(ChatColor.translateAlternateColorCodes('&', "&fand Redstone are on offer!"));
		ItemStack item7 = createItem(Material.DIAMOND_ORE, ChatColor.translateAlternateColorCodes('&', "&aPublic Mine"), lore5);

		List<String> lore6 = new ArrayList<String>();
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&fFree open-to-all."));
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&fAnvils & Enchanting"));
		lore6.add(ChatColor.translateAlternateColorCodes('&', "&ftables available here!"));
		ItemStack item8 = createItem(Material.ENCHANTING_TABLE, ChatColor.translateAlternateColorCodes('&', "&aPublic Enchanting"), lore6);
		
		List<String> lore7 = new ArrayList<String>();
		lore7.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore7.add(ChatColor.translateAlternateColorCodes('&', "&fEnter your Username and"));
		lore7.add(ChatColor.translateAlternateColorCodes('&', "&frecieve a Voter Crate Key,"));
		lore7.add(ChatColor.translateAlternateColorCodes('&', "&f$150 and a Vote Credit!"));
		ItemStack item9 = createItem(Material.EMERALD, ChatColor.translateAlternateColorCodes('&', "&aVoting"), lore7);
		
		List<String> lore8 = new ArrayList<String>();
		lore8.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore8.add(ChatColor.translateAlternateColorCodes('&', "&fJobs are a stable source"));
		lore8.add(ChatColor.translateAlternateColorCodes('&', "&fof income on Survival!"));
		lore8.add("");
		lore8.add(ChatColor.translateAlternateColorCodes('&', "&c&oRight Click to join Job!"));
		ItemStack item10 = createItem(Material.STONE_PICKAXE, ChatColor.translateAlternateColorCodes('&', "&aJobs"), lore8);
		
		List<String> lore9 = new ArrayList<String>();
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&f&oCommands:"));
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&f  /claim &8(Claim 5x5 square around you)"));
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&f  /abandonclaim &8(Unclaim claimed plot.)"));
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&f  /trust <player> &8(Allow player to build)"));
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&f  /untrust <player> &8(Remove player from trust list)"));
		lore9.add(ChatColor.translateAlternateColorCodes('&', "&f  /claimslist&8(Lists all claimed areas.)"));
		ItemStack item11 = createItem(Material.STICK, ChatColor.translateAlternateColorCodes('&', "&aLand Claiming"), lore9);
		
		List<String> lore10 = new ArrayList<String>();
		lore10.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore10.add(ChatColor.translateAlternateColorCodes('&', "&fGain extra perks with each rank!"));
		lore10.add(ChatColor.translateAlternateColorCodes('&', "&fExchange your $$ for Ranks here."));
		ItemStack item12 = createItem(Material.BOOK, ChatColor.translateAlternateColorCodes('&', "&aRanks"), lore10);
		
		List<String> lore11 = new ArrayList<String>();		
		lore11.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore11.add(ChatColor.translateAlternateColorCodes('&', "&fMore homes, new in-game"));
		lore11.add(ChatColor.translateAlternateColorCodes('&', "&ftag. Click to find out how"));
		lore11.add(ChatColor.translateAlternateColorCodes('&', "&fto become a Veteran!"));
		ItemStack item13 = createItem(Material.GOLDEN_HELMET, ChatColor.translateAlternateColorCodes('&', "&aVeteran Rank"), lore11);
		
		List<String> lore14 = new ArrayList<String>();
		lore14.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore14.add(ChatColor.translateAlternateColorCodes('&', "&fView a variety of Kits here!"));
		lore14.add(ChatColor.translateAlternateColorCodes('&', "&fDaily, Weekly and Rank kits available."));
		lore14.add(ChatColor.translateAlternateColorCodes('&', "&fUse &a/credits&f to purchase more Kits!"));
		ItemStack item14 = createItem(Material.ARMOR_STAND, ChatColor.translateAlternateColorCodes('&', "&aKit Master"), lore14);

		List<String> lore15 = new ArrayList<String>();
		lore15.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
		lore15.add(ChatColor.translateAlternateColorCodes('&', "&fExchange Vote Credits here!"));
		lore15.add(ChatColor.translateAlternateColorCodes('&', "&fKits, Crate Keys, Commands & Perks!"));
		ItemStack item15 = createItem(Material.GOLD_NUGGET, ChatColor.translateAlternateColorCodes('&', "&aCredit Master"), lore15);
		
		ItemStack item4 = createItem(Material.BARRIER, ChatColor.translateAlternateColorCodes('&', "&eClose GUI"));
		
		ClickableItem itemMachineClick = ClickableItem.of(itemMachine, e -> {
			player.closeInventory();
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "im openbook " + player.getName());
		});
		ClickableItem item0Click = ClickableItem.of(item0, e -> {
		});
		ClickableItem item1Click = ClickableItem.of(item1, e -> {
			player.closeInventory();
			player.sendMessage(ChatColor.GRAY + "Teleporting you to random location...");
			player.performCommand("wild");
		});
		ClickableItem item3Click = ClickableItem.of(item3, e -> {
			player.closeInventory();
			DonateInventory.INVENTORY.open(player);
			player.sendMessage(ChatColor.RED + "Type " + ChatColor.AQUA + "/buy" + ChatColor.RED + " in chat to view our Shop!");
		});
		
		ClickableItem item2Click = ClickableItem.of(item2, e -> {
			player.closeInventory();
			player.performCommand("guild");
		});
		ClickableItem item4Click = ClickableItem.of(item5, e -> {
			player.closeInventory();
			player.performCommand("member");
		});
		ClickableItem item5Click = ClickableItem.of(item6, e -> {
			player.closeInventory();
			player.performCommand("shops");
		});
		ClickableItem item6Click = ClickableItem.of(item7, e -> {
			player.closeInventory();
			player.performCommand("warp mine");
		});
		ClickableItem item7Click = ClickableItem.of(item8, e -> {
			player.closeInventory();
			player.performCommand("warp enchant");
		});
		ClickableItem item8Click = ClickableItem.of(item9, e -> {
			player.closeInventory();
			player.performCommand("vote");
		});
		ClickableItem item9Click = ClickableItem.of(item10, e -> {
			player.closeInventory();
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Use command &a/jobs browse&7 to view the GUI list of Jobs!"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Use command &a/jobs&7 for help!"));
		});
		ClickableItem item10Click = ClickableItem.of(item11, e -> {

		});
		ClickableItem item11Click = ClickableItem.of(item12, e -> {
			player.closeInventory();
			player.performCommand("ranks");
		});
		ClickableItem item12Click = ClickableItem.of(item13, e -> {
			player.closeInventory();
			player.performCommand("veteran");
		});
		ClickableItem item14Click = ClickableItem.of(item14, e -> {
			player.closeInventory();
			player.performCommand("kits");
		});
		ClickableItem item15Click = ClickableItem.of(item15, e -> {
			player.closeInventory();
			CreditMasterInventory.INVENTORY.open(player);
		});

		cont.set(1, 2, item11Click);
		cont.set(1, 3, item0Click);
		cont.set(1, 4, item8Click);
		cont.set(1, 5, item9Click);
		cont.set(1, 6, item5Click);
		cont.set(2, 2, item10Click);
		cont.set(2, 3, item7Click);
		cont.set(2, 4, item4Click);
		cont.set(2, 5, item12Click);
		cont.set(2, 6, item1Click);
		cont.set(3, 2, item14Click);
		cont.set(3, 3, itemMachineClick);
		cont.set(3, 4, item2Click);
		cont.set(3, 5, item6Click);
		cont.set(3, 6, item15Click);
		cont.set(4, 4, item3Click);
		cont.set(4, 7, ClickableItem.of(item4, e -> player.closeInventory()));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {

	}

	private ItemStack createItem(Material m, String name) {
		ItemStack item = new ItemStack(m);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		item.setItemMeta(meta);
		return item;
	}
	
	private ItemStack createItem(Material m, String name, List<String> lores) {
		ItemStack item = new ItemStack(m);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		meta.setLore(lores);
		item.setItemMeta(meta);
		return item;
	}
}
