package com.kronuscraftmc.inventorys.donate;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.shops.BaseInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class CurrencyInventory extends BaseInventory {
	private static final int ROW = 0;
	private static final int COLUMN = 1;
	public static final SmartInventory INVENTORY = SmartInventory.builder().id("dcurrency").provider(new CurrencyInventory())
			.size(4, 9).title(ChatColor.RED + "Currency GUI").build();

	
	enum Shop {
		TWENTYFIVE (
				"&a&l$25,000",
				new String [] { "&f$25,000 usable in-game money!"},
				Material.PAPER,
				new int[] {1,1} 
				),
		FIFTY (
				"&a&l$50,000",
				new String [] { "&f$50,000 usable in-game money!"},
				Material.PAPER,
				new int[] {1,3}
				),
		HUNDRED (
				"&a&l$100,000",
				new String [] { "&f$10,000 usable in-game money!"},
				Material.PAPER,
				new int[] {1,5}
				),
		TWOFIFTY (
				"&a&l$250,000",
				new String [] { "&f$250,000 usable in-game money!"},
				Material.PAPER ,
				new int[] {1,7} 
			),
		FIVEHUNDRED (
				"&a&l$500,000",
				new String [] { "&f$500,000 usable in-game money!"},
				Material.PAPER ,
				new int[] {2,4} 
			);
		
		public final String name;
		public final String[] loreLines;
		public final Material guiObject;
		public final int[] containerCoordinates;
		Shop(String title, String[] loreLines, Material guiObject, int[] containerCoordinates){
			this.name = title;
			this.loreLines = loreLines;
			this.guiObject = guiObject;
			this.containerCoordinates = containerCoordinates;
		}
}

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		for (Shop shop : Shop.values()) {
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.translateAlternateColorCodes('&', "&b&oClick to view link!"));
			lore.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
			for (String loresLines : shop.loreLines) {
				lore.add(ChatColor.translateAlternateColorCodes('&', loresLines));
			}
			ItemStack item = BaseInventory.createItem(shop.guiObject, shop.name, lore);
			ClickableItem icon = ClickableItem.of(item, e -> {
				player.closeInventory();
				player.sendMessage("");
				player.sendMessage("");
				player.sendMessage("");
				player.sendMessage(ChatColor.GRAY + "Click here: " + ChatColor.AQUA + "http://kronuscraft.buycraft.net/category/1271902");
				player.sendMessage(ChatColor.RED + "Type " + ChatColor.AQUA + "/buy" + ChatColor.RED + " in chat to view our Shop!");
			});

			cont.set(shop.containerCoordinates[ROW], shop.containerCoordinates[COLUMN], icon);
		}
		ItemStack gui = BaseInventory.createItem(Material.BARRIER, ChatColor.RED + "Back to Donate GUI");
		cont.set(2, 7, ClickableItem.of(gui, e -> DonateInventory.INVENTORY.open(player)));
	}
	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}
}
