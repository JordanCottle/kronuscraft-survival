package com.kronuscraftmc.shops;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.Survival;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class ShopNav extends BaseInventory {
	private static final int ROW = 0;
	private static final int COLUMN = 1;
	
	public static final SmartInventory NULL_INVENTORY = null;

	public static final SmartInventory INVENTORY = SmartInventory.builder().id("shop").provider(new ShopNav())
			.size(4, 9).title(ChatColor.GREEN + "Shop GUI").build();

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));

		// create the shop items and tie them to their respective shops
		HashMap<String, BaseInventory> shops = Survival.getInstance().getShopManager().getShops();
		for(String shopName: shops.keySet()){
			BaseInventory shop = shops.get(shopName);
			ClickableItem icon = ClickableItem.of(shop.icon, e -> {
				player.closeInventory();
				shop.inventory.open(player);
			});
			cont.set(shop.navCoordinates[ROW], shop.navCoordinates[COLUMN], icon);
		}

		// create the cancel button
		ItemStack gui = createItem(Material.BARRIER, ChatColor.RED + "Close GUI");
		cont.set(2, 7, ClickableItem.of(gui, e -> player.closeInventory()));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}

	
}
