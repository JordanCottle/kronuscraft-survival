package com.kronuscraftmc.inventorys.donate;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.shops.BaseInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class PerksInventory extends BaseInventory {
	private static final int ROW = 0;
	private static final int COLUMN = 1;
	public static final SmartInventory INVENTORY = SmartInventory.builder().id("dperks").provider(new PerksInventory())
			.size(4, 9).title(ChatColor.RED + "Perks GUI").build();

	
	enum Shop {
		MCMMO2 (
				"&a&lDouble McMMO Exp Boost",
				new String [] { "&f2x the normal amount of McMMO Exp earned!", "&7&oThis perk is perminant!"},
				Material.TRIPWIRE_HOOK,
				new int[] {1,2} 
				),
		MCMMO3 (
				"&a&lTriple McMMO Exp Boost",
				new String [] { "&f3x the normal amount of McMMO Exp earned!", "&7&oThis perk is perminant!"},
				Material.TRIPWIRE_HOOK,
				new int[] {1,4}
				),
		MCMMO4 (
				"&a&lQuadruple McMMO Exp Boost",
				new String [] {"&f4x the normal amount of McMMO Exp earned!", "&7&oThis perk is perminant!"},
				Material.TRIPWIRE_HOOK,
				new int[] {1,6}
			);
		
		public final String name;
		public final String[] loreLines;
		public final Material guiObject;
		public final int[] containerCoordinates;
		Shop(String title, String[] loreLines, Material guiObject, int[] containerCoordinates){
			this.name = title;
			this.loreLines = loreLines;
			this.guiObject = guiObject;
			this.containerCoordinates = containerCoordinates;
		}
}

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		for (Shop shop : Shop.values()) {
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.translateAlternateColorCodes('&', "&b&oClick to view link!"));
			lore.add(ChatColor.translateAlternateColorCodes('&', "&8-+-+-"));
			for (String loresLines : shop.loreLines) {
				lore.add(ChatColor.translateAlternateColorCodes('&', loresLines));
			}
			ItemStack item = BaseInventory.createItem(shop.guiObject, shop.name, lore);
			ClickableItem icon = ClickableItem.of(item, e -> {
				player.closeInventory();
				player.sendMessage("");
				player.sendMessage("");
				player.sendMessage("");
				player.sendMessage(ChatColor.GRAY + "Click here: " + ChatColor.AQUA + "http://kronuscraft.buycraft.net/category/1271898");
				player.sendMessage(ChatColor.RED + "Type " + ChatColor.AQUA + "/buy" + ChatColor.RED + " in chat to view our Shop!");
			});

			cont.set(shop.containerCoordinates[ROW], shop.containerCoordinates[COLUMN], icon);
		}
		ItemStack gui = BaseInventory.createItem(Material.BARRIER, ChatColor.RED + "Back to Donate GUI");
		cont.set(2, 7, ClickableItem.of(gui, e -> DonateInventory.INVENTORY.open(player)));
	}
	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}
}
