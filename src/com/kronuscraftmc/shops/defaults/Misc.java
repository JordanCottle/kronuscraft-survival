package com.kronuscraftmc.shops.defaults;

import org.bukkit.configuration.file.FileConfiguration;

public enum Misc {
	SADDLE("SADDLE", 250.0, 100.0),
	SHULKER_SHELL("SHULKER_SHELL", 2000.0, 250.0),
	HEART_OF_THE_SEA("HEART_OF_THE_SEA", 5000.0, 500.0),
	NAUTILUS_SHELL("NAUTILUS_SHELL", 2500.0, 250.0),
	TRIDENT("TRIDENT", 20000.0, 1000.0),
	BEACON("BEACON", 10000.0, 500.0),
	TOTEM_OF_UNDYING("TOTEM_OF_UNDYING", 5000.0, 500.0),
	ELYTRA("ELYTRA", 7500.0, 500.0),
	DRAGON_BREATH("DRAGON_BREATH", 500.0, 50.0),
	NETHER_STAR("NETHER_STAR", 10000.0, 500.0),
	IRON_HORSE_ARMOR("IRON_HORSE_ARMOR", 200.0, 50.0),
	GOLDEN_HORSE_ARMOR("GOLDEN_HORSE_ARMOR", 250.0, 75.0),
	DIAMOND_HORSE_ARMOR("DIAMOND_HORSE_ARMOR", 400.0, 125.0),
	ENCHANTING_TABLE("ENCHANTING_TABLE", 400.0, 50.0),
	PAPER("PAPER", 2.0, 1.0),
	BOOK("BOOK", 5.0, 2.5),
	BOOKSHELF("BOOKSHELF", 15.0, 5.0),
	ARMOR_STAND("ARMOR_STAND", 50.0, 10.0),
	NAME_TAG("NAME_TAG", 25.0, 5.0);
	
	public final String name;
	public final double buyPrice;
	public final double sellPrice;
	Misc(String name, double buyPrice, double sellPrice){
		this.name = name;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
	}
	
	public static void setUp(FileConfiguration fileConfig) {
		for(Misc item: Misc.values()) {
			fileConfig.addDefault(item.name + ".BuyPrice", item.buyPrice);
			fileConfig.addDefault(item.name + ".SellPrice", item.sellPrice);
		}
	}
}