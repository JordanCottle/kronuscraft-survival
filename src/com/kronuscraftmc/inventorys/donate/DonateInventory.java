package com.kronuscraftmc.inventorys.donate;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.kronuscraftmc.inventorys.HelpInventory;
import com.kronuscraftmc.shops.BaseInventory;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class DonateInventory extends BaseInventory {
	private static final int ROW = 0;
	private static final int COLUMN = 1;
	public static final SmartInventory INVENTORY = SmartInventory.builder().id("donator").provider(new DonateInventory()).size(4, 9).title(ChatColor.RED + "Donate GUI").build();

	
	enum Shop {
		RANKS (
				"&aRanks",
				new String [] { "&8-+-+-", "&fView a list of Global", "&fNetwork Ranks here!"},
				Material.PLAYER_HEAD,
				new int[] {2,4} 
				),
		CURRENCY(
				"&aCurrency",
				new String [] { "&8-+-+-", "&fUsable in-game currency", "&favailable here!"},
				Material.GOLD_INGOT,
				new int[] {1,3}
				),
		COMMANDS (
				"&aCommands",
				new String [] { "&8-+-+-", "&fView a list of usable", "&fcommands & Perks here!"},
				Material.PAPER,
				new int[] {1,5} 
				),
		CRATEKEYS (
				"&aCrate Keys",
				new String [] { "&8-+-+-", "&fKeys available for use", "&fon Crates at spawn!"},
				Material.TRIPWIRE_HOOK,
				new int[] {1,7} 
				),
		PERKS (
				"&aPerks",
				new String [] { "&8-+-+-", "&fView a list of Perks here!"},
				Material.ARMOR_STAND,
				new int[] {1,1} 
			);
		
		public final String name;
		public final String[] loreLines;
		public final Material guiObject;
		public final int[] containerCoordinates;
		Shop(String title, String[] loreLines, Material guiObject, int[] containerCoordinates){
			this.name = title;
			this.loreLines = loreLines;
			this.guiObject = guiObject;
			this.containerCoordinates = containerCoordinates;
		}
}

	@Override
	public void init(Player player, InventoryContents cont) {
		ItemStack glass = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
		ItemMeta glassMeta = glass.getItemMeta();
		glassMeta.setDisplayName(ChatColor.RED + "");
		glass.setItemMeta(glassMeta);
		cont.fillBorders(ClickableItem.empty(glass));
		for (Shop shop : Shop.values()) {
			List<String> lore = new ArrayList<String>();
			for (String loresLines : shop.loreLines) {
				lore.add(ChatColor.translateAlternateColorCodes('&', loresLines));
			}
			ItemStack item = BaseInventory.createItem(shop.guiObject, shop.name, lore);
			ClickableItem icon = ClickableItem.of(item, e -> {
				player.closeInventory();
				switch(shop.name) {
				case "&aPerks":
					PerksInventory.INVENTORY.open(player);
					break;
				case "&aCrate Keys":
					CrateKeyInventory.INVENTORY.open(player);
					break;
				case "&aCommands":
					CommandsInventory.INVENTORY.open(player);
					break;
				case "&aCurrency":
					CurrencyInventory.INVENTORY.open(player);
					break;
				case "&aRanks":
					RanksInventory.INVENTORY.open(player);
					break;
				}
			});

			cont.set(shop.containerCoordinates[ROW], shop.containerCoordinates[COLUMN], icon);
		}
		ItemStack gui = BaseInventory.createItem(Material.BARRIER, ChatColor.RED + "Back to Help GUI");
		cont.set(2, 7, ClickableItem.of(gui, e -> HelpInventory.INVENTORY.open(player)));
	}
	@Override
	public void update(Player arg0, InventoryContents arg1) {
		
	}
}
