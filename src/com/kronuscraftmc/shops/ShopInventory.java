package com.kronuscraftmc.shops;

import com.kronuscraftmc.Survival;
import com.kronuscraftmc.shops.defaults.Shop;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryContents;

public class ShopInventory extends BaseInventory {
	private final BuyableItem[] items;
	private final String title;
	private final String name;
	
	private final String SHOP_TITLE_COLOR = ChatColor.AQUA.toString();
	
	ShopInventory(Shop shop, ShopConfig config){
		this.name = shop.name;
		this.title = SHOP_TITLE_COLOR + name + " Shop";
		this.items = config.getItems();
		super.navCoordinates = shop.containerCoordinates;
		
		super.icon = createItem(shop.guiObject, title, createLore(SHOP_DESCRIPTION_COLOR, NAV_HEADING, shop.loreLines));
		
		super.inventory = SmartInventory.builder().id(name).provider(this)
				.size(5, 9).title(title).build();
	}

	@Override
	public void init(Player player, InventoryContents container) {
		ShopManager manager = Survival.getInstance().getShopManager();
		int row = 0;
		int col = 0;
		
		int buyAmount = manager.getBuyAmount(player);
		boolean sellAll = manager.getSellAll(player);
		for(BuyableItem item: items) {
			ClickableItem icon = ClickableItem.of(item.getIcon(buyAmount), e -> {
				if(e.isLeftClick()){
					sellItem(player, item, buyAmount);
				}
				else if(e.isRightClick()){
					if(sellAll) {
						buyAll(player, item);
					} else {
						buyItem(player, item, buyAmount);
					}
				}
			});
			
			container.set(row, col++, icon);
			if (col > 8) {
				col = 0;
				row++;
				if(row > 3) {
					log("Too many items!");
					break;
				}
			}
		}
		
		// create change amount button
		ItemStack accumulator = createItem(Material.NETHER_STAR, ChatColor.GREEN + "Change Amount", buyAmount);
		container.set(4, 7, ClickableItem.of(accumulator, e -> {
			if(e.isLeftClick()){
				manager.changeBuyAmount(player, 1);
				this.inventory.open(player);
			}
			else if(e.isRightClick()){
				manager.changeBuyAmount(player, -1);
				this.inventory.open(player);
			}
		}));
		
		
		// create sell all button
		container.set(4,  6, ClickableItem.of(createSellAllButton(sellAll), e -> {
			manager.toggleSellAll(player);
			this.inventory.open(player);
		}));

		//create back button
		ItemStack backItem = createItem(Material.ARROW, "Back");
		container.set(4, 0, ClickableItem.of(backItem, e -> ShopNav.INVENTORY.open(player)));

		// create the cancel button
		ItemStack closeItem = createItem(Material.BARRIER, ChatColor.RED + "Close GUI");
		container.set(4, 8, ClickableItem.of(closeItem, e -> player.closeInventory()));
	}

	@Override
	public void update(Player arg0, InventoryContents arg1) {
	}
	
	private ItemStack createSellAllButton(boolean sellAll) {
		// create sell all button
		Material sellAllMaterial;
		String sellAllDescription;
		ChatColor sellAllColor;
		if(sellAll) {
			sellAllMaterial = Material.GREEN_CONCRETE;
			sellAllDescription = "Sell all is ON";
			sellAllColor = ChatColor.GREEN;
		}else {
			sellAllMaterial = Material.RED_CONCRETE;
			sellAllDescription = "Sell all is OFF";
			sellAllColor = ChatColor.DARK_RED;
		}
		
		return createItem(sellAllMaterial, sellAllColor + sellAllDescription);
	}
}
