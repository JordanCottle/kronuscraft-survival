package com.kronuscraftmc.shops;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Stores data for the Shop class to use when creating its GUI
 * @author Jordan Cottle
 *
 */
public class BuyableItem {
	private final String LOG_HEADER = "[BuyableItem] ";
	public final String name;
	private Material material;
	private final double unitBuyPrice;
	private final double unitSellPrice;

	private static final String INFO_COLOR_CODE = "&e";
	private static final String BUY_PRICE_COLOR_CODE = "&c&o";  // red + italics
	private static final String SELL_PRICE_COLOR_CODE = "&a&o"; // green + italics
	
	/**
	 * Creates a basic BuyableItem from an item's name, buy price, and sell price
	 * @param name The name of an item, as passed into Material.getMaterial(name) to retrieve the item's Material
	 * @param buyPrice The amount a player pays to buy the item
	 * @param sellPrice	The amount a player receives from selling the item
	 */
	public BuyableItem(String name, double buyPrice, double sellPrice) {
		this.name = name;
		this.unitBuyPrice = buyPrice;
		this.unitSellPrice = sellPrice;
		
		material = Material.getMaterial(name);
		
		if(material == null) {
			log("Material: " + name + " creation failed!");
			material = Material.BARRIER;
		}
	}

	/**
	 * Creates the actual item a player would purchase/sell
	 * 
	 * @param stackAmount The number of this item that will be purchased/sold at a time
	 * @return The ItemStack that contains the specified number of this item in it
	 */
	public ItemStack getItem(int stackAmount){
		return new ItemStack(material, stackAmount);
	}

	/**
	 * Creates the icon to de displayed in the shop
	 * 
	 * @param stackAmount The number of this item that will be purchased/sold at a time
	 * @return	The ItemStack with lore reflecting the total prices for this stack of items
	 */
	public ItemStack getIcon(int stackAmount){
		return createItem(material, createLore(getBuyPrice(stackAmount), getSellPrice(stackAmount)), stackAmount);
	}

	/**
	 * Calculates the total cost of ordering a specified number of this item
	 * 
	 * @param sellAmount The number of this item the shop is going to sell
	 * @return The total cost of buying a given amount of this item
	 */
	public double getBuyPrice(int sellAmount){
		return unitBuyPrice * sellAmount;
	}

	/**
	 * Calculates the total amount earned for selling a specified number of this item
	 * 
	 * @param buyAmount The number of this item the shop is going to buy
	 * @return The total amount of money the player will receive for selling the given number of this item
	 */
	public double getSellPrice(int buyAmount){
		return unitSellPrice * buyAmount;
	}
	
	public String toString() {
		return String.format("%s {Buy: %.2f, Sell: %.2f}", name, unitBuyPrice, unitSellPrice);
	}


	private void log(String message) {
		System.out.println(LOG_HEADER + message);
	}

	/**
	 * Creates a new ItemStack from a material and list of lores for the description
	 * 
	 * @param material The material to use for the new item
	 * @param lores The description/lore of the item to be created
	 * @return The ItemStack of the specified material with given lore attached to it
	 */
	private ItemStack createItem(Material material, List<String> lores, int amount) {
		ItemStack item = new ItemStack(material, amount);
		ItemMeta meta = item.getItemMeta();
		meta.setLore(lores);
		item.setItemMeta(meta);
		return item;
	}

	/**
	 * Creates lore for a shop item using the buy and sell prices for the item
	 * 
	 * @param buyPrice The amount a player pays when buying the item from the store
	 * @param sellPrice The amount a player receives for selling the item to the store
	 * @return
	 */
	private List<String> createLore(double buyPrice, double sellPrice){
		List<String> lore = new ArrayList<String>();

		lore.add(ChatColor.translateAlternateColorCodes('&', String.format("%sBuy Price: %s%.2f", INFO_COLOR_CODE, BUY_PRICE_COLOR_CODE, buyPrice)));
		lore.add(ChatColor.translateAlternateColorCodes('&', String.format("%sSell Price: %s%.2f", INFO_COLOR_CODE, SELL_PRICE_COLOR_CODE, sellPrice)));
		lore.add(ChatColor.translateAlternateColorCodes('&', ""));
		lore.add(ChatColor.translateAlternateColorCodes('&', "&7&oLeft Click to buy!"));
		lore.add(ChatColor.translateAlternateColorCodes('&', "&7&oRight Click to sell!"));

		return lore;
	}
}
