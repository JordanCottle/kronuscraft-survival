package com.kronuscraftmc;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class VoteParty {

	private final int votesStarter = 30;
	private int votes;
	private FileConfiguration file = Survival.getInstance().getConfig();
	
	public VoteParty() {
		this.votes = file.getInt("Votes");
	}
	
	public int getRemainingVotes() {
		return votesStarter - votes;
	}
	
	public void addVote() {
		votes++;
		if(votes == votesStarter) {
			votePartySetup();
		}
		file.set("Votes", votes);
		Survival.getInstance().saveConfig();
	}
	
	private void votePartySetup() {
		votes = 0;
		for(Player player : Bukkit.getOnlinePlayers()) {
			player.sendMessage("");
			player.sendMessage("");
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&lThe Drop Party will be triggered in 1 minute!"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&fRewards given to all:"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x64 Coal"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x64 Iron Ingot"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x64 Gold Ingot"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x32 Diamond"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x16 Emerald"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x2 Voter Crate Key"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x1 Mystery Crate Key"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + x1 Epic Crate Key"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + 2 Vote Credits"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7 + $2,500 In-Game Money"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&oEnsure inventory has space!"));
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(Survival.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				for(Player player : Bukkit.getOnlinePlayers()) {
					SurvivalPlayer sPlayer = SurvivalPlayerManager.getPlayer(player.getUniqueId());
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + player.getPlayer().getName() + " voter 2");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + player.getPlayer().getName() + " mystery 1");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate key " + player.getPlayer().getName() + " epic 1");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + player.getPlayer().getName() + " 2500");
					ItemStack[] items = { new ItemStack(Material.COAL, 64), new ItemStack(Material.IRON_INGOT, 64), new ItemStack(Material.GOLD_INGOT, 64),
							new ItemStack(Material.DIAMOND, 32), new ItemStack(Material.EMERALD, 16)};
					for(ItemStack is : items) {
						player.getInventory().addItem(is);
					}
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&fYour Vote Party rewards have been awarded!"));
					if(sPlayer != null) {
						sPlayer.getGamePlayer().setVoteCredits(sPlayer.getGamePlayer().getVoteCredits() + 2);
					}
				}
			}
		}, 20 * 60);
	}
}
