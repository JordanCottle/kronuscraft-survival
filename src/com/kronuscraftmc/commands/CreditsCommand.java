package com.kronuscraftmc.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.kronuscraftmc.inventorys.credits.CreditMasterInventory;

public class CreditsCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command c, String l, String[] args) {
		if(sender instanceof Player) {
			CreditMasterInventory.INVENTORY.open(((Player) sender));
		}
		return true;
	}

}
